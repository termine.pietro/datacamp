# Import the logistic regression
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.feature_extraction.text import CountVectorizer,ENGLISH_STOP_WORDS
from sklearn.model_selection import train_test_split   
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from textblob import TextBlob
from wordcloud import WordCloud
from data import *
movies_df = pd.read_csv('IMDB_sample.csv')

print(movies_df.head())
vect = CountVectorizer(max_features=200)
vect1 = CountVectorizer(max_features=200, ngram_range=(1, 2),stop_words=ENGLISH_STOP_WORDS) #max_df=500)
vect.fit(movies_df.review)
vect1.fit(movies_df.review)
movies_sparse = vect.transform(movies_df.review)
movies_sparse1 = vect.transform(movies_df.review)

movies  = pd.DataFrame(movies_sparse.toarray(), columns=vect.get_feature_names())
movies1  = pd.DataFrame(movies_sparse1.toarray(), columns=vect1.get_feature_names())

print(movies.head())
print(movies1.head())
# Define the vector of targets and matrix of features
y = movies_df.label
X = movies#.drop('label', axis=1)
X1 = movies1#.drop('label', axis=1)

# Build a logistic regression model and calculate the accuracy
log_reg = LogisticRegression().fit(X, y)
log_reg1 = LogisticRegression().fit(X1, y)
print('Accuracy of logistic regression: ', log_reg.score(X, y))
print('Accuracy of logistic regression: ', log_reg.score(X1, y))


# Define the vector of labels and matrix of features
#y = movies.label
#X = movies.drop('label', axis=1)

# Perform the train-test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
X1_train, X1_test, y1_train, y1_test = train_test_split(X1, y, test_size=0.2, random_state=42)

# Build a logistic regression model and print out the accuracy
log_reg = LogisticRegression().fit(X_train,y_train)
log_reg = LogisticRegression().fit(X1_train,y1_train)
print('Accuracy on train set: ', log_reg.score(X_train,y_train))
print('Accuracy on train set: ', log_reg.score(X_test,y_test))
print('Accuracy on test set: ', log_reg.score(X1_train, y1_train))
print('Accuracy on test set: ', log_reg.score(X1_test, y1_test))

# Split into training and testing
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=321)

# Train a logistic regression
log_reg = LogisticRegression().fit(X_train,y_train)

# Predict the probability of the 0 class
prob_0 = log_reg.predict_proba(X_test)[:, 0]
# Predict the probability of the 1 class
prob_1 = log_reg.predict_proba(X_test)[:, 1]

print("First 10 predicted probabilities of class 0: ", prob_0[:10])
print("First 10 predicted probabilities of class 1: ", prob_1[:10])


print("")