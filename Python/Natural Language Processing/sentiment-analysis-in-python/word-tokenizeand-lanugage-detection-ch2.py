import pandas as pd
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from textblob import TextBlob
from wordcloud import WordCloud
from data import *

from sklearn.feature_extraction.text import CountVectorizer
from nltk import word_tokenize
# Import the language detection function and package
from langdetect import detect_langs


reviews = pd.read_csv('amazon_reviews_sample.csv')
reviews1 = reviews.drop(["Unnamed: 0" ],axis=1)
print(reviews.head())
print(word_tokenize(GoT))

# Tokenize each item in the avengers 
tokens_avengers = [word_tokenize(item) for item in avengers]

print(tokens_avengers)

# Tokenize each item in the review column 
word_tokens = [word_tokenize(review) for review in reviews.review]

# Print out the first item of the word_tokens list
print(word_tokens[0])

# Create an empty list to store the length of reviews
len_tokens = []

# Iterate over the word_tokens list and determine the length of each item
for i in range(len(word_tokens)):
     len_tokens.append(len(word_tokens[i]))

# Create a new feature for the lengh of each review
reviews['n_words'] = len_tokens 

print(reviews.head())

# Detect the language of the foreign string
print(detect_langs(foreign))


languages = []

# Loop over the sentences in the list and detect their language
for sentence in range(len(sentences)):
    languages.append(detect_langs(sentences[sentence]))
    
print('The detected languages are: ', languages)

languages = [] 

# Loop over the rows of the dataset and append  
for row in range(len(reviews1)):
    languages.append(detect_langs(reviews1.iloc[row, 1]))

# Clean the list by splitting     
languages = [str(lang).split(':')[0][1:] for lang in languages]

# Assign the list to a new feature 
reviews1['language'] = languages

reviews1.to_csv('reviews_with_detected_lang.csv')
print(reviews1.head())