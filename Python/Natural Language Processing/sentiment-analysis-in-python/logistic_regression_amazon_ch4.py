import pandas as pd
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from textblob import TextBlob
from wordcloud import WordCloud
from data import *

from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score,confusion_matrix
from nltk import word_tokenize
from sklearn.metrics import accuracy_score
from sklearn.feature_extraction.text import CountVectorizer,ENGLISH_STOP_WORDS
from sklearn.model_selection import train_test_split

# Import the language detection function and package
from langdetect import detect_langs


reviews = pd.read_csv('amazon_reviews_sample.csv')
reviews1 = reviews.drop(["Unnamed: 0" ],axis=1)
print(reviews.head())
my_pattern = r'\b[^\d\W][^\d\W]+\b'
vect = CountVectorizer(max_features=200, ngram_range=(1, 2),stop_words=ENGLISH_STOP_WORDS) #max_df=500)
vect1 = TfidfVectorizer(ngram_range=(1, 2), max_features=100, token_pattern=my_pattern, stop_words=ENGLISH_STOP_WORDS)
vect.fit(reviews.review)
vect1.fit(reviews.review)
review_sparse = vect.transform(reviews.review)
review_sparse1 = vect1.transform(reviews.review)

X  = pd.DataFrame(review_sparse.toarray(), columns=vect.get_feature_names())
X1  = pd.DataFrame(review_sparse1.toarray(), columns=vect1.get_feature_names())

y = reviews.score.values

# Split the data into training and testing
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
X_train1, X_test1, y_train1, y_test1 = train_test_split(X1, y, test_size=0.3, random_state=42)

# Build a logistic regression
log_reg = LogisticRegression().fit(X_train,y_train)
log_reg1 = LogisticRegression().fit(X_train1,y_train1)

# Predict the labels 
y_predict = log_reg.predict(X_test)
y_predict1 = log_reg1.predict(X_test1)

# Print the performance metrics
print('Accuracy score of test data: ', accuracy_score(y_test, y_predict))
print('Confusion matrix of test data: \n', confusion_matrix(y_test, y_predict)/len(y_test)) 

print('Accuracy score of test data: ', accuracy_score(y_test1, y_predict1))
print('Confusion matrix of test data: \n', confusion_matrix(y_test1, y_predict1)/len(y_test1)) 

# Split data into training and testing
X_train, X_test,y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)

# Train a logistic regression with regularization of 1000 'l2' param
log_reg1 = LogisticRegression(penalty='l2',C=1000).fit(X_train, y_train)
# Train a logistic regression with regularization of 0.001
log_reg2 = LogisticRegression(penalty='l2',C=0.001).fit(X_train, y_train)

# Print the accuracies
print('Accuracy of model 1: ', log_reg1.score(X_test, y_test))
print('Accuracy of model 2: ', log_reg2.score(X_test, y_test))

positive_reviews_vec = reviews[reviews.score == 1].review.values
positive_reviews = ""
for s in positive_reviews_vec:
    positive_reviews += s
# Create and generate a word cloud image
cloud_positives = WordCloud(background_color='white').generate(positive_reviews)
 
# Display the generated wordcloud image
plt.imshow(cloud_positives, interpolation='bilinear') 
plt.axis("off")

# Don't forget to show the final image
plt.show()

# Tokenize each item in the review column
word_tokens = [word_tokenize(review) for review in reviews.review]

# Create an empty list to store the length of the reviews
len_tokens = []

# Iterate over the word_tokens list and determine the length of each item
for i in range(len(word_tokens)):
     len_tokens.append(len(word_tokens[i]))

# Create a new feature for the lengh of each review
reviews['n_words'] = len_tokens 

# Build the vectorizer
vect = TfidfVectorizer(stop_words=ENGLISH_STOP_WORDS, ngram_range=(1, 2), max_features=200, token_pattern=r'\b[^\d\W][^\d\W]+\b').fit(reviews.review)
# Create sparse matrix from the vectorizer
X = vect.transform(reviews.review)

# Create a DataFrame
reviews_transformed = pd.DataFrame(X.toarray(), columns=vect.get_feature_names())
print('Top 5 rows of the DataFrame: \n', reviews_transformed.head())

# Define X and y
y = reviews.score
X = reviews_transformed#.drop('score', axis=1)

# Train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=456)

# Train a logistic regression
log_reg = LogisticRegression().fit(X_train, y_train)
# Predict the labels
y_predicted = log_reg.predict(X_test)

# Print accuracy score and confusion matrix on test set
print('Accuracy on the test set: ', accuracy_score(y_test, y_predicted))
print(confusion_matrix(y_test, y_predicted)/len(y_test))