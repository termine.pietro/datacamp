# Import the logistic regression
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.feature_extraction.text import CountVectorizer,ENGLISH_STOP_WORDS
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score,confusion_matrix

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from textblob import TextBlob
from wordcloud import WordCloud
from data import *


tweets_df = pd.read_csv('Tweets.csv')
print(tweets_df.text.head())

mapping = {"positive":2.0,"negative":0.0,"neutral":1.0}
y = tweets_df['airline_sentiment'].map(mapping)
 
print(y.head())

# Define the stop words
my_stop_words = ENGLISH_STOP_WORDS.union(['airline', 'airlines', '@', 'am', 'pm'])
 
# Build and fit the vectorizers
vect = CountVectorizer(max_features=200,stop_words=ENGLISH_STOP_WORDS) 
vect.fit(tweets_df.text)

# Define the vector of targets and matrix of features
y = tweets_df.airline_sentiment
#X = tweets_df.drop('airline_sentiment', axis=1).dropna()
X = vect.transform(tweets_df.text)
# Build a logistic regression model and calculate the accuracy
log_reg = LogisticRegression().fit(X,y)
print('Accuracy of logistic regression: ', log_reg.score(X,y))

# Create an array of prediction
y_predict = log_reg.predict(X)

# Print the accuracy using accuracy score
print('Accuracy of logistic regression: ', accuracy_score(y, y_predict))

# Define the vector of targets and matrix of features
#y = tweets.airline_sentiment
#X = tweets.drop('airline_sentiment', axis=1)

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=123, stratify=y)

# Train a logistic regression
log_reg = LogisticRegression().fit(X_train, y_train)

# Make predictions on the test set
y_predicted = log_reg.predict(X_test)

# Print the performance metrics
print('Accuracy score test set: ', accuracy_score(y_test, y_predicted))
print('Confusion matrix test set: \n', confusion_matrix(y_test, y_predicted)/len(y_test))

# Build a logistic regression with regularizarion parameter of 100
log_reg1 = LogisticRegression(penalty='l2',C=100).fit(X_train, y_train)
# Build a logistic regression with regularizarion parameter of 0.1
log_reg2 = LogisticRegression(penalty='l2',C=0.1).fit(X_train, y_train)

# Predict the labels for each model
y_predict1 = log_reg1.predict(X_test)
y_predict2 = log_reg2.predict(X_test)

# Print performance metrics for each model
print('Accuracy of model 1: ', accuracy_score(y_test,y_predict1))
print('Accuracy of model 2: ', accuracy_score(y_test,y_predict2))
print('Confusion matrix of model 1: \n' , confusion_matrix(y_test, y_predict1)/len(y_test))
print('Confusion matrix of model 2: \n',  confusion_matrix(y_test, y_predict2)/len(y_test))