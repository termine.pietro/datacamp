from collections import Counter
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import nltk
import os
import glob


def find_files_with_exention(folder,file_ext):
    pattern = folder+"/*"+file_ext
    files = glob.glob(pattern)
    return files

def preprocess_file(filename):
    article = ""
    with open(filename,'r',encoding="utf-8") as f:
        article = f.read()
    #print(article)
    # Tokenize the article: tokens
    tokens = word_tokenize(article)

    # Convert the tokens into lowercase: lower_tokens
    lower_tokens = [t.lower() for t in tokens]

    # Retain alphabetic words: alpha_only
    alpha_only = [t for t in lower_tokens if t.isalpha()]
    # Remove all stop words: no_stops
    no_stops = [t for t in alpha_only if t not in stopwords.words('english')]
    
    return no_stops


def preprocess_all(folder,file_ext):
    files = find_files_with_exention(folder,file_ext)
    list_txt = [preprocess_file(f) for f in files]
    return list_txt
