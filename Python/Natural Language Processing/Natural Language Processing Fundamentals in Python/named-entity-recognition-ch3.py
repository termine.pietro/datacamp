# better library for named entinty recognition: Stanford CoreNLP
import nltk
import matplotlib.pyplot as plt
from collections import defaultdict
#nltk.download('averaged_perceptron_tagger')
#nltk.download('maxent_ne_chunker')
#nltk.download('words')

import spacy

article = ""
with open('uber_apple.txt','r',encoding='utf-8') as f:
    article = f.read()
# Tokenize the article into sentences: sentences
sentences = nltk.sent_tokenize(article)

# Tokenize each sentence into words: token_sentences
token_sentences = [nltk.word_tokenize(sent) for sent in sentences]

# Tag each tokenized sentence into parts of speech: pos_sentences
pos_sentences = [nltk.pos_tag(sent) for sent in token_sentences] 

# Create the named entity chunks: chunked_sentences
chunked_sentences = nltk.ne_chunk_sents(pos_sentences,binary=True)


# Test for stems of the tree with 'NE' tags
for sent in chunked_sentences:
    for chunk in sent:
        if hasattr(chunk, "label") and chunk.label() == "NE":
            print(chunk)



articles = ""
with open('articles.txt','r',encoding='utf-8') as f:
    articles = f.read()
# Tokenize the article into sentences: sentences
sentences = nltk.sent_tokenize(article)

# Tokenize each sentence into words: token_sentences
token_sentences = [nltk.word_tokenize(sent) for sent in sentences]

# Tag each tokenized sentence into parts of speech: pos_sentences
pos_sentences = [nltk.pos_tag(sent) for sent in token_sentences] 

# Create the named entity chunks: chunked_sentences
chunked_sentences = nltk.ne_chunk_sents(pos_sentences,binary=False)
# Create the defaultdict: ner_categories
#ner_categories = defaultdict(default_factory=int)
ner_categories =  defaultdict(int)

# Create the nested for loop
for sent in chunked_sentences:
    for chunk in sent:
        if hasattr(chunk, 'label'):
            ner_categories[chunk.label()] += 1
            
# Create a list from the dictionary keys for the chart labels: labels
labels = list(ner_categories.keys())

# Create a list of the values: values
values = [ner_categories.get(l) for l in labels]

# Create the pie chart
plt.pie(values, labels=labels, autopct='%1.1f%  %', startangle=140)
# Display the chart
plt.show()

print("Introduction to SpaCy")
print("language packs required using python -m spacy download en")

# Instantiate the English model: nlp
nlp = spacy.load('en',tagger=False, parser=False, matcher=False)

# Create a new document: doc
doc = nlp(article)

# Print all of the found entities and their labels
for ent in doc.ents:
    print(ent.label_, ent.text)


## Polyglot cant install it 
# Create a new text object using Polyglot's Text class: txt
# article = "french txt file"
# txt = Text(article)
# 
# # Print each of the entities found
# for ent in txt.entities:
#     print(ent)
#     
# # Print the type of ent
# print(type(ent))

# Create the list of tuples: entities

#entities = [(ent.tag,' '.join(ent )) for ent in txt.entities]
#
## Print entities
#print(entities)

# Initialize the count variable: count
# count = 0
# 
# # Iterate over all the entities
# for ent in txt.entities:
#     # Check whether the entity contains 'Márquez' or 'Gabo'
#     if('Márquez' in  ent or  'Gabo' in ent):
#         # Increment count
#         count += 1 
# 
# # Print count
# print(count)
# 
# # Calculate the percentage of entities that refer to "Gabo": percentage
# percentage = count / len(txt.entities)
# print(percentage)


print("")