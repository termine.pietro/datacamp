import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn.preprocessing import normalize
from scipy.cluster.hierarchy import fcluster
from sklearn.manifold import TSNE


df0 = pd.read_csv('seeds.csv')
samples = df0.iloc[:,0:7].values
varieties = df0.iloc[:,7].values
mapping = {1: "Canadian wheat",2:"Kama wheat",3:"Rosa wheat"}

v1 = []
for v in varieties:
    v1.append(mapping[v])

varieties = v1

# Calculate the linkage: mergings
mergings = linkage(samples,method='complete')

# Plot the dendrogram, using varieties as labels
dendrogram(mergings,
           labels=varieties,
           leaf_rotation=90,
           leaf_font_size=6)
plt.show()



df3 = pd.read_csv('company-stock-movements-2010-2015-incl.csv')
print(df3.head())
companies = df3.iloc[:,0].values
movements = df3.iloc[:,1:].values
# Normalize the movements: normalized_movements
normalized_movements = normalize(movements)

# Calculate the linkage: mergings
mergings = linkage(normalized_movements,method='complete')

# Plot the dendrogram
dendrogram(mergings,
           labels=companies,
           leaf_rotation=90,
           leaf_font_size=6)
plt.show()



# Create a TSNE instance: model
model = TSNE(learning_rate=50)
normalized_movements = normalize(movements)
# Apply fit_transform to normalized_movements: tsne_features
tsne_features = model.fit_transform(normalized_movements)

# Select the 0th feature: xs
xs = tsne_features[:,0]

# Select the 1th feature: ys
ys = tsne_features[:,1]

# Scatter plot
plt.scatter(xs,ys,alpha=0.5)

# Annotate the points
for x, y, company in zip(xs, ys, companies):
    plt.annotate(company, (x, y), fontsize=5, alpha=0.75)
plt.show()


# apin in the ass to make it usable
# Calculate the linkage: mergings
#mergings = linkage(samples,method='single')
#
## Plot the dendrogram
#dendrogram(mergings,
#           labels=country_names,
#           leaf_rotation=90,
#           leaf_font_size=6)
#plt.show()
samples = df0.iloc[:,0:7].values

# Calculate the linkage: mergings
mergings = linkage(samples,method='complete')

# Plot the dendrogram, using varieties as labels
dendrogram(mergings,
           labels=varieties,
           leaf_rotation=90,
           leaf_font_size=6)
plt.show()

# Use fcluster to extract labels: labels
labels = fcluster(mergings,6,criterion='distance')

# Create a DataFrame with labels and varieties as columns: df
df = pd.DataFrame({'labels': labels, 'varieties': varieties})

# Create crosstab: ct
ct = pd.crosstab(df['labels'],df['varieties'])

# Display ct
print(ct)




variety_numbers = df0.iloc[:,7].values

# Create a TSNE instance: model
model = TSNE(learning_rate=200)

# Apply fit_transform to samples: tsne_features
tsne_features = model.fit_transform(samples)

# Select the 0th feature: xs
xs = tsne_features[:,0]

# Select the 1st feature: ys
ys = tsne_features[:,1]

# Scatter plot, coloring by variety_numbers
plt.scatter(xs,ys,c=variety_numbers)
plt.show()






print("")