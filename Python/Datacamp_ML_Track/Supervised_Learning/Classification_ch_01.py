import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets

from sklearn.model_selection import train_test_split


#plt.figure()
#sns.countplot(x='education', hue='party', data=df, palette='RdBu')
#plt.xticks([0,1], ['No', 'Yes'])
#plt.show()


# excericse 1: K-NN classifier
file = "house-votes-84.csv"
#load files and fill nans with previous values
# and add columns names (indices)
col_names = ['party', 'infants', 'water', 'budget', 'physician', 'salvador',
           'religious', 'satellite', 'aid', 'missile', 'immigration',
           'synfuels', 'education', 'superfund', 'crime', 'duty_free_exports',
           'eaa_rsa']

mapping = {'y': 1, 'n': 2,'?':np.nan}

df = pd.read_csv(file,names=col_names) 
print(df.head())
df = df.replace('?', np.NaN)
print(df.head())


df.replace({'(?<![\w\d])y(?![\w\d])': 1}, regex=True,inplace=True) 
df.replace({'(?<![\w\d])n(?![\w\d])': 0}, regex=True,inplace=True)  
df.fillna(inplace=True,method='bfill')
df.fillna(inplace=True,method='ffill')
print(df.head())

# Import KNeighborsClassifier from sklearn.neighbors

# Create arrays for the features and the response variable
y = df['party'].values
X = df.drop('party', axis=1).values

# Create a k-NN classifier with 6 neighbors

knn = KNeighborsClassifier(n_neighbors=6)
print( np.isnan(X))


print( np.sum(np.isnan(X)))
# Fit the classifier to the data
knn.fit(X,y) 


X_new = np.array([[0.696469,  0.286139,  0.226851,  0.551315,  0.719469,  0.423106,  0.980764,   
                  0.68483,  0.480932,  0.392118,  0.343178,  0.72905,  0.438572,  0.059678,   
                  0.398044,  0.737995]])

print(X_new)
print(X_new.shape)

# Predict the labels for the training data X
y_pred = knn.predict(X)

# Predict and print the label for the new data point X_new
new_prediction = knn.predict(X_new)
print("Prediction: {}".format(new_prediction))




# Load the digits dataset: digits
digits = datasets.load_digits()

# Print the keys and DESCR of the dataset
print(digits.keys())
print(digits['DESCR'])

# Print the shape of the images and data keys
print(digits.images.shape)
print(digits.data.shape)

# Display digit 1010
plt.imshow(digits.images[1010], cmap=plt.cm.gray_r, interpolation='nearest')
plt.show()



# Create feature and target arrays
X = digits.data
y = digits.target

# Split into training and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=42, stratify=y)

# Create a k-NN classifier with 7 neighbors: knn
knn = KNeighborsClassifier(n_neighbors=7)

# Fit the classifier to the training data
knn.fit(X_train,y_train)

# Print the accuracy
print(knn.score(X_test,y_test))


# Setup arrays to store train and test accuracies
neighbors = np.arange(1, 9)
train_accuracy = np.empty(len(neighbors))
test_accuracy = np.empty(len(neighbors))

# Loop over different values of k
for i, k in enumerate(neighbors):
    # Setup a k-NN Classifier with k neighbors: knn
    knn = KNeighborsClassifier(n_neighbors=k)

    # Fit the classifier to the training data
    knn.fit(X_train,y_train)
    
    #Compute accuracy on the training set
    train_accuracy[i] = knn.score(X_train, y_train)

    #Compute accuracy on the testing set
    test_accuracy[i] = knn.score(X_test, y_test)

# Generate plot
plt.title('k-NN: Varying Number of Neighbors')
plt.plot(neighbors, test_accuracy, label = 'Testing Accuracy')
plt.plot(neighbors, train_accuracy, label = 'Training Accuracy')
plt.legend()
plt.xlabel('Number of Neighbors')
plt.ylabel('Accuracy')
plt.show()

print("")