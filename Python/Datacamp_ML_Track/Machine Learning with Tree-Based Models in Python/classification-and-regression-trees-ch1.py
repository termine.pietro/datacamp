from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from mlxtend.plotting import plot_decision_regions

def plot_labeled_decision_regions1(X,y,clfs,lab):
    print('Sitl bug skippoed for now!!!')
    #pass
    len_clfs = len(clfs)
    #fig, axes = plt.subplots(nrows=1, ncols=len_clfs,figsize=(8, 4))
    fig, axes = plt.subplots(1, len_clfs, figsize=(10, 3))
  
    #fig = plot_decision_regions(X=X, y=y, clf=clf2, ax=axes[1], legend=1)
    ctr = 0
    for clf in clfs:
        #fig = plot_decision_regions(X=X.values, y=y.values,clf=clf, legend=2)
        fig = plot_decision_regions(X=X.values, y=y.values, clf=clf, ax=axes[ctr], legend=ctr+1)
        plt.title(type(clf))
        ctr += 1
    plt.show()

    
def plot_labeled_decision_regions(X,y, models):    
    '''
    Function producing a scatter plot of the instances contained 
    in the 2D dataset (X,y) along with the decision 
    regions of two trained classification models contained in the
    list 'models'.
            
    Parameters
    ----------
    X: pandas DataFrame corresponding to two numerical features 
    y: pandas Series corresponding the class labels
    models: list containing two trained classifiers 
    
    '''
    if len(models) != 2:
        raise Exception('''
        Models should be a list containing only two trained classifiers.
        ''')
    if not isinstance(X, pd.DataFrame):
        raise Exception('''
        X has to be a pandas DataFrame with two numerical features.
        ''')
    if not isinstance(y, pd.Series):
        raise Exception('''
        y has to be a pandas Series corresponding to the labels.
        ''')
    fig, ax = plt.subplots(1, 2, figsize=(6.0,2.7), sharey=True)
    for i, model in enumerate(models):
        plot_decision_regions(X.values,y.values, model, legend= 2, ax = ax[i])
        ax[i].set_title(model.__class__.__name__)
        ax[i].set_xlabel(X.columns[0])
        if i == 0:
            ax[i].set_ylabel(X.columns[1])
        ax[i].set_ylim(X.values[:,1].min(), X.values[:,1].max())
        ax[i].set_xlim(X.values[:,0].min(), X.values[:,0].max())
    plt.tight_layout()
    plt.show()

SEED = 1
df = pd.read_csv('wbc.csv')
print(df.head())

X = df[['radius_mean',  'concave points_mean']]
y = df['diagnosis']
value_mapper = {'M':1,'B':0}
y = y.map(value_mapper)

# Instantiate a DecisionTreeClassifier 'dt' with a maximum depth of 6
dt = DecisionTreeClassifier(max_depth=6, random_state=SEED)

X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=SEED)
# Fit dt to the training set
dt.fit(X_train, y_train)

# Predict test set labels
y_pred = dt.predict(X_test)
print(y_pred[0:5])

# Compute test set accuracy  
acc = accuracy_score(y_test, y_pred)
print("Test set accuracy: {:.2f}".format(acc))

# Instatiate logreg
logreg = LogisticRegression(random_state=1)

# Fit logreg to the training set
logreg.fit(X_train,y_train)

# Define a list called clfs containing the two classifiers logreg and dt
clfs = [logreg, dt]


# for plotting decision regions 
#http://rasbt.github.io/mlxtend/user_guide/plotting/plot_decision_regions/
# Review the decision regions of the two classifiers
plot_labeled_decision_regions(X_test, y_test, clfs)

import inspect
lines = inspect.getsource(plot_labeled_decision_regions)
print(lines)


# Instantiate dt_entropy, set 'entropy' as the information criterion
dt_entropy = DecisionTreeClassifier(max_depth=8, criterion='entropy', random_state=1)

# Fit dt_entropy to the training set
dt_entropy.fit(X_train, y_train)
