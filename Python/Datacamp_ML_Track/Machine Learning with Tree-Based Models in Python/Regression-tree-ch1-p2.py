import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE



SEED = 1
df = pd.read_csv('auto.csv')
print(df.head())
# create categorical clolumnsa 
y = df['mpg']
X = pd.concat([df.drop(['mpg','origin'],axis=1), pd.get_dummies(df.origin, prefix='origin')], axis=1)

print(df.head())
print(X.head())
print(y.head())



X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=SEED)

lr = LinearRegression()
# Instantiate dt
dt = DecisionTreeRegressor(max_depth=8,
             min_samples_leaf=0.13, # maximum % of samples features to be contained in each node
            random_state=3)

# Fit dt to the training set
dt.fit(X_train, y_train)
lr.fit(X_train,y_train)

# Compute y_pred
y_pred = dt.predict(X_test)

# Compute mse_dt
mse_dt = MSE(y_pred, y_test)

# Compute rmse_dt
rmse_dt = mse_dt ** (1/2)

# Print rmse_dt
print("Test set RMSE of dt: {:.2f}".format(rmse_dt))

print("\n\nLinear Regression VS Regression Tree \n\n")

# Predict test set labels 
y_pred_lr = lr.predict(X_test)

# Compute mse_lr
mse_lr = MSE(y_test, y_pred_lr)

# Compute rmse_lr
rmse_lr = mse_lr ** (1/2)

# Print rmse_lr
print('Linear Regression test set RMSE: {:.2f}'.format(rmse_lr))

# Print rmse_dt
print('Regression Tree test set RMSE: {:.2f}'.format(rmse_dt))