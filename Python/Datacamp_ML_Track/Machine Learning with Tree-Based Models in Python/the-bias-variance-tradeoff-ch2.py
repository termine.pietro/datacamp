import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE
from sklearn.model_selection import cross_val_score 

SEED = 1
df = pd.read_csv('auto.csv')
print(df.head())
# create categorical clolumnsa 
y = df['mpg']
X = pd.concat([df.drop(['mpg','origin'],axis=1), pd.get_dummies(df.origin, prefix='origin')], axis=1)

print(df.head())
print(X.head())
print(y.head())


# Split the data into 70% train and 30% test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=SEED)

# Instantiate a DecisionTreeRegressor dt
dt = DecisionTreeRegressor(max_depth=4, min_samples_leaf=0.26, random_state=SEED)

# Compute the array containing the 10-folds CV MSEs
MSE_CV_scores = - cross_val_score(dt, X_train, y_train, cv=10, 
                       scoring='neg_mean_squared_error', # cv doesn't handle regular mse but why?
                       n_jobs=-1) # use all cpus

# Compute the 10-folds CV RMSE
RMSE_CV = (MSE_CV_scores.mean())**(1/2) # this is calculated as mean 

# Print RMSE_CV
print('CV RMSE: {:.2f}'.format(RMSE_CV))


# Fit dt to the training set
dt.fit(X_train, y_train)

# Predict the labels of the training set
y_pred_train = dt.predict(X_train)

# Evaluate the training set RMSE of dt
RMSE_train = (MSE(y_train,y_pred_train))**(0.5)

# Print RMSE_train
print('Train RMSE: {:.2f}'.format(RMSE_train))