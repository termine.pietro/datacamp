# Import GridSearchCV
from sklearn.model_selection import GridSearchCV
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE
# Import RandomForestRegressor
from sklearn.ensemble import RandomForestRegressor



SEED = 1
df = pd.read_csv('bikes.csv')
print(df.head())
# create categorical clolumnsa 
y = df['cnt']
X = df.drop(['cnt'],axis=1)

print(df.head())
print(X.head())
print(y.head())



X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=SEED)



# Instantiate rf
rf = RandomForestRegressor(n_estimators=25, # number of trees
            random_state=SEED)

# Instantiate grid_rf
params_rf = {'n_estimators' :[ 100, 350, 500],'max_features' :['log2', 'auto', 'sqrt'], 'min_samples_leaf' : [ 2, 10, 30]}
grid_rf = GridSearchCV(estimator=rf,
                       param_grid=params_rf,
                       scoring='neg_mean_squared_error',
                       cv=3,
                       verbose=1,
                       n_jobs=-1)

grid_rf.fit(X_train,y_train)

# Extract the best estimator
best_model = grid_rf.best_estimator_

# Predict test set labels
y_pred = best_model.predict(X_test)

# Compute rmse_test
rmse_test = MSE(y_pred,y_test) ** 0.5

# Print rmse_test
print('Test RMSE of best model: {:.3f}'.format(rmse_test)) 