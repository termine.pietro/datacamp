
# Import GradientBoostingRegressor
from sklearn.ensemble import GradientBoostingRegressor
# Import GradientBoostingRegressor
from sklearn.ensemble import GradientBoostingRegressor
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE
# Import RandomForestRegressor
from sklearn.ensemble import RandomForestRegressor



SEED = 2
df = pd.read_csv('bikes.csv')
print(df.head())
# create categorical clolumnsa 
y = df['cnt']
X = df.drop(['cnt'],axis=1)

print(df.head())
print(X.head())
print(y.head())



X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=SEED)


# Instantiate sgbr
sgbr = GradientBoostingRegressor(max_depth=4, 
            subsample=0.9,    # maximum samples to be used for trainig each  tree
            max_features=0.75, # maximum samples to be used for each tree to perform best split 
            n_estimators=200,                                
            random_state=SEED)
# Fit sgbr to the training set
sgbr.fit(X_train,y_train)

# Predict test set labels
y_pred = sgbr.predict(X_test)


# Compute test set MSE
mse_test = MSE(y_pred,y_test)

# Compute test set RMSE
rmse_test = mse_test ** 0.5

# Print rmse_test
print('Test set RMSE of sgbr: {:.3f}'.format(rmse_test))