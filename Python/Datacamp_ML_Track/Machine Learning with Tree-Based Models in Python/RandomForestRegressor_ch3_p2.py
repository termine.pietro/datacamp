import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE
# Import RandomForestRegressor
from sklearn.ensemble import RandomForestRegressor



SEED = 1
df = pd.read_csv('bikes.csv')
print(df.head())
# create categorical clolumnsa 
y = df['cnt']
X = df.drop(['cnt'],axis=1)

print(df.head())
print(X.head())
print(y.head())



X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=SEED)



# Instantiate rf
rf = RandomForestRegressor(n_estimators=25, # number of trees
            random_state=2)
            
# Fit rf to the training set    
rf.fit(X_train, y_train) 

# Predict the test set labels
y_pred = rf.predict(X_test)

# Evaluate the test set RMSE
rmse_test = MSE(y_test,y_pred) ** 0.5

# Print rmse_test
print('Test set RMSE of rf: {:.2f}'.format(rmse_test))

# Create a pd.Series of features importances
importances = pd.Series(data=rf.feature_importances_,
                        index= X_train.columns)

# Sort importances
importances_sorted = importances.sort_values()

# Draw a horizontal barplot of importances_sorted
importances_sorted.plot(kind='barh', color='lightgreen')
plt.title('Features Importances')
plt.show()