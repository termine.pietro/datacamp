from keras.layers import Input, Dense
from keras.models import Model
from keras.utils import plot_model
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np 



# Input layer
input_tensor = Input(shape=(1,))

# Dense layer
output_layer = Dense(1)

# Connect the dense layer to the input_tensor
output_tensor = output_layer(input_tensor)

# Create a dense layer and connect the dense layer to the input_tensor in one step
# Note that we did this in 2 steps in the previous exercise, but are doing it in one step now
output_tensor = Dense(1)(input_tensor)

# Build the model
model = Model(input_tensor, output_tensor)
# Compile the model
model.compile(optimizer='adam', loss='mean_absolute_error')


# Summarize the model
model.summary()

# Plot the model
plot_model(model, to_file='model.png')

# Display the image
data = plt.imread('model.png')
plt.imshow(data)
plt.show()

from pandas import read_csv
games = read_csv('games_tourney.csv')
games_tourney_test = read_csv('games_season_enriched.csv')

# Now fit the model
model.fit(games['seed_diff'], games['score_diff'],
          epochs=1,
          batch_size=128,
          validation_split=0.1,
          verbose=True)

# Load the X variable from the test data
X_test = games_tourney_test['seed_diff']

# Load the y variable from the test data
y_test = games_tourney_test['score_diff']

# Evaluate the model on the test data
print(model.evaluate(X_test, y_test, verbose=False))

print()