
# Import necessary modules
import matplotlib.pyplot as plt
import networkx as nx
from nxviz import MatrixPlot
import matplotlib.pyplot as plt


G = nx.read_gpickle('github_users.p')
# Plot the degree distribution of the GitHub collaboration network
plt.hist((list(nx.degree_centrality(G).values())))
plt.show()


# Plot the degree distribution of the GitHub collaboration network
plt.hist((list(nx.betweenness_centrality(G).values())))
plt.show()

# Import necessary modules


# Calculate the largest connected component subgraph: largest_ccs
largest_ccs = sorted(nx.connected_component_subgraphs(G), key=lambda x: len(x))[-1]

# Create the customized MatrixPlot object: h
h = MatrixPlot(largest_ccs,'grouping')

# Draw the MatrixPlot to the screen
h.draw()
plt.show()


# Iterate over all the nodes in G, including the metadata
for n, d in  G.nodes(data=True):

    # Calculate the degree of each node: G.node[n]['degree']
    G.node[n]['degree'] = nx.degree(G,n)

# Create the ArcPlot object: a
a = ArcPlot(G,'degree')

# Draw the ArcPlot to the screen
a.draw()
plt.show()

# Iterate over all the nodes, including the metadata
for n, d in G.nodes(data=True):

    # Calculate the degree of each node: G.node[n]['degree']
    G.node[n]['degree'] = nx.degree(G,n)

# Create the CircosPlot object: c
c = CircosPlot(G,node_order='degree',node_grouping='grouping',node_color='grouping')

# Draw the CircosPlot object to the screen
c.draw()
plt.show()

# Calculate the maximal cliques in G: cliques
cliques = nx.find_cliques(G)

# Count and print the number of maximal cliques in G
print(len(list(cliques)))



# Find the author(s) that are part of the largest maximal clique: largest_clique
largest_clique = sorted(list(nx.find_cliques(G)), key=lambda x:len(x))[-1]

# Create the subgraph of the largest_clique: G_lc
G_lc = G.subgraph(largest_clique)

# Create the CircosPlot object: c
c = CircosPlot(G_lc)

# Draw the CircosPlot to the screen
c.draw()
plt.show()

#Finding important collaborators

#Almost there! You'll now look at important nodes once more. Here, you'll make use of the degree_centrality() 
##and betweenness_centrality() functions in NetworkX to compute each of the respective centrality scores, and 
##then use that information to find the "important nodes". In other words, your job in this exercise is to find #the user(s) that have collaborated with the most number of users.

# Compute the degree centralities of G: deg_cent
deg_cent = nx.degree_centrality(G)

# Compute the maximum degree centrality: max_dc
max_dc = max(list(deg_cent.values()))

# Find the user(s) that have collaborated the most: prolific_collaborators
prolific_collaborators = [n for n, dc in deg_cent.items() if dc == max_dc]

# Print the most prolific collaborator(s)
print(prolific_collaborators)




#  Go out 1 degree of separation from the clique, and add those users to the subgraph. Inside the first for loop:
#      Add nodes to G_lmc from the neighbors of G using the .add_nodes_from() and .neighbors() methods.
#      Using the .add_edges_from(), method, add edges to G_lmc between the current node and all its neighbors. To do this, you'll have create a list of tuples using the zip() function consisting of the current node and each of its neighbors. The first argument to zip() should be [node]*len(list(G.neighbors(node))), and the second argument should be the neighbors of node.
#  Record each node's degree centrality score in its node metadata.
#      Do this by assigning nx.degree_centrality(G_lmc)[n] to G_lmc.node[n]['degree centrality'] in the second for loop.
#  Visualize this network with an ArcPlot sorting the nodes by degree centrality (you can do this using the keyword argument node_order='degree centrality').
#Characterizing editing communities : max clique

#You're now going to combine what you've learned about the BFS algorithm and concept of maximal cliques to #visualize the network with an ArcPlot.

#The largest maximal clique in the Github user collaboration network has been assigned to the subgraph G_lmc #Note that for NetworkX version 2.x and later, G.subgraph(nodelist) returns only an immutable view on the #original graph. We must explicitly ask for a .copy() of the graph to obtain a mutatable version.

# Import necessary modules
from nxviz import ArcPlot
import matplotlib.pyplot as plt

# Identify the largest maximal clique: largest_max_clique
largest_max_clique = set(sorted(nx.find_cliques(G), key=lambda x: len(x))[-1])

# Create a subgraph from the largest_max_clique: G_lmc
G_lmc = G.subgraph(largest_max_clique).copy()  

# Go out 1 degree of separation
for node in list(G_lmc.nodes()):
    G_lmc.add_nodes_from(G.neighbors(node))
    G_lmc.add_edges_from(zip([node]*len(list(G.neighbors(node))), G.neighbors(node)))

# Record each node's degree centrality score
for n in G_lmc.nodes():
    G_lmc.node[n]['degree centrality'] = nx.degree_centrality(G_lmc)[n]

# Create the ArcPlot object: a
a = ArcPlot(G_lmc,node_order='degree centrality')

# Draw the ArcPlot to the screen
a.draw()
plt.show()

# Recommending co-editors who have yet to edit together
# 
# Finally, you're going to leverage the concept of open triangles to recommend users on GitHub to collaborate!
# Instructions
# 100 XP
# 
#     Compile a list of GitHub users that should be recommended to collaborate with one another. To do this:
#         In the first for loop, iterate over all the nodes in G, including the metadata (by specifying data=True).
#         In the second for loop, iterate over all the possible triangle combinations, which can be identified using the combinations() function with a size of 2.
#         If n1 and n2 do not have an edge between them, a collaboration between these two nodes (users) should be recommended, so increment the (n1), (n2) value of the recommended dictionary in this case. You can check whether or not n1 and n2 have an edge between them using the .has_edge() method.
#     Using a list comprehension, identify the top 10 pairs of users that should be recommended to collaborate. The iterable should be the key-value pairs of the recommended dictionary (which can be accessed with the .items() method), while the conditional should be satisfied if count is greater than the top 10 in all_counts. Note that all_counts is sorted in ascending order, so you can access the top 10 with all_counts[-10].
# Import necessary modules
from itertools import combinations
from collections import defaultdict

# Initialize the defaultdict: recommended
recommended = defaultdict(int)

# Iterate over all the nodes in G
for n, d in G.nodes(data=True):

    # Iterate over all possible triangle relationship combinations
    for n1, n2 in combinations(G.neighbors(n), 2):

        # Check whether n1 and n2 do not have an edge
        if not G.has_edge(n1, n2):

            # Increment recommended
            recommended[(n1, n2)] += 1

# Identify the top 10 pairs of users
all_counts = sorted(recommended.values())
top10_pairs = [pair for pair, count in recommended.items() if count > all_counts[-10]]
print(top10_pairs)
