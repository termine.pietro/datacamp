
# Import FunctionTransformer
from sklearn.preprocessing import FunctionTransformer
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from multilabel import multilabel_train_test_split

from sklearn.linear_model import LogisticRegression
from sklearn.multiclass   import OneVsRestClassifier
from sklearn.feature_extraction.text import CountVectorizer


NUMERIC_COLUMNS = ['FTE', 'Total']
LABELS=['Function','Use','Sharing','Reporting','Student_Type',
        'Position_Type','Object_Type','Pre_K','Operating_Status']

# Define combine_text_columns()
def combine_text_columns(data_frame, to_drop=NUMERIC_COLUMNS + LABELS):
    """ converts all text in each row of data_frame to single vector """
    
    # Drop non-text columns that are in the df
    to_drop = set(to_drop) & set(data_frame.columns.tolist())
    text_data = data_frame.drop(to_drop,axis=1)
    
    # Replace nans with blanks
    text_data.fillna('',inplace=True)
    
    # Join all text items in a row that have a space in between
    return text_data.apply(lambda x: " ".join(x), axis=1)

df = pd.read_csv("TrainingData.csv",index_col=0)
df[["FTE", "Total"]] = df[["FTE", "Total"]].apply(pd.to_numeric)
print(df.head())
print(df.info())

# Get the dummy encoding of the labels
dummy_labels = pd.get_dummies(df[LABELS])

# Get the columns that are features in the original df
NON_LABELS = [c for c in df.columns if c not in LABELS]

# Split into training and test sets
X_train, X_test, y_train, y_test = multilabel_train_test_split(df[NON_LABELS],
                                                               dummy_labels,
                                                               0.2, 
                                                               seed=123)

# Preprocess the text data: get_text_data
get_text_data = FunctionTransformer(combine_text_columns, validate=False)

# Preprocess the numeric data: get_numeric_data
get_numeric_data = FunctionTransformer(lambda x: x[NUMERIC_COLUMNS], validate=False)
