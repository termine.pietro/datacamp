import pandas as pd
import matplotlib.pyplot as plt
import numpy as np  
# Q 1

# Import pandas

# Read the file into a DataFrame: df
airquality = pd.read_csv('airquality.csv')
df = pd.read_csv('airquality.csv')

# Print the head of df
print(df.head())
print(airquality.head())


# Melt airquality: airquality_melt
airquality_melt = pd.melt(df, id_vars=['Month','Day'])

# Print the head of airquality_melt
print(airquality_melt.head())


# Rename columns: Melt airquality: airquality_melt
airquality_melt_with_names = pd.melt(airquality, id_vars=['Month', 'Day'], var_name='measurement', value_name='reading')

# Print the head of airquality_melt
print(airquality_melt_with_names.head())



# Pivot airquality_melt by using .pivot_table() with the rows indexed by 'Month' and 'Day', the columns # # indexed by 'measurement', and the values populated with 'reading'.
# Pivot airquality_melt: airquality_pivot
airquality_pivot = airquality_melt_with_names.pivot_table(index=['Month', 'Day'], columns='measurement', values='reading')

# Print the head of airquality_pivot
print(airquality_pivot.head())

# Print the index of airquality_pivot
print(airquality_pivot.index)

# Reset the index of airquality_pivot: airquality_pivot_reset
airquality_pivot_reset = airquality_pivot.reset_index()

# Print the new index of airquality_pivot_reset
print(airquality_pivot_reset.index)

# Print the head of airquality_pivot_reset
print(airquality_pivot_reset.head())

airquality_dup = airquality_melt_with_names

# Pivot table the airquality_dup: airquality_pivot
airquality_pivot = airquality_dup.pivot_table(index=['Month', 'Day'], 
    columns='measurement', values='reading', aggfunc=np.mean)

# Print the head of airquality_pivot before reset_index
print(airquality_pivot.head())

# Reset the index of airquality_pivot
airquality_pivot = airquality_pivot.reset_index()

# Print the head of airquality_pivot
print(airquality_pivot.head())

# Print the head of airquality
print(airquality.head())


# questo e' il pobelma che avevo io prima
tb = pd.read_csv('tb.csv')


# dividing age roup m14, m1424 and son on in separate columns 
# coontaining the age and sex separtetl and not agegroup thingy agove 

# Melt tb: tb_melt
tb_melt = pd.melt(frame=tb, id_vars=['country','year']) # id_vars cols to keep

# Create the 'gender' column
tb_melt['gender'] = tb_melt.variable.str[0]

# Create the 'age_group' column
tb_melt['age_group'] = tb_melt.variable.str[1:]

# Print the head of tb_melt
print(tb_melt.head())

ebola = pd.read_csv("ebola.csv")
# Melt ebola: ebola_melt
ebola_melt = pd.melt(ebola, id_vars=['Date', 'Day'], var_name='type_country', value_name='counts')

print(ebola_melt.type_country.str.split('_'))


# Melt ebola: ebola_melt
ebola_melt = pd.melt(ebola, id_vars=['Date', 'Day'], var_name='type_country', value_name='counts')

# Create the 'str_split' column
ebola_melt['str_split'] = ebola_melt.type_country.str.split("_")

# Create the 'type' column
ebola_melt['type'] = ebola_melt.str_split.str.get(0)

# Create the 'country' column
ebola_melt['country'] = ebola_melt.str_split.str.get(1)

# Print the head of ebola_melt
print(ebola_melt.head())


print(":")
