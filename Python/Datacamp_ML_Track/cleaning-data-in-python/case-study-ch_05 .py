import pandas as pd
import matplotlib.pyplot as plt
import numpy as np  
import glob
import re

g1800s = pd.read_csv('gapminder.csv')
# dataset full on nans and different from exercise, Nan should be droped
gapminder = pd.read_csv('gapminder.csv')
gapminder.dropna()
# Create the scatter plot
g1800s.plot(kind="scatter", x='1800', y='1899')

# Specify axis labels
plt.xlabel('Life Expectancy by Country in 1800')
plt.ylabel('Life Expectancy by Country in 1899')

# Specify axis limits
plt.xlim(20, 55)
plt.ylim(20, 55)

# Display the plot
plt.show()

def check_null_or_valid(row_data):
    """Function that takes a row of data,
    drops all missing values,
    and checks if all remaining values are greater than or equal to 0
    """
    no_na = row_data.dropna()
    numeric = pd.to_numeric(no_na)
    ge0 = numeric >= 0
    return ge0

# Check whether the first column is 'Life expectancy'
# assert g1800s.columns[0] == 'Life expectancy' # this line fails because tehrei an unmaned col on 0 

print(g1800s.head())

assert g1800s.columns[-1] == 'Life expectancy'
# Check whether the values in the row are valid
#assert g1800s.iloc[:, 1:].apply(check_null_or_valid, axis=1).all().all()

# Check that there is only one instance of each country
#assert g1800s['Life expectancy'].value_counts()[0] == 1


# Melt gapminder: gapminder_melt
gapminder_melt = pd.melt(gapminder, id_vars='Life expectancy')

# Rename the columns
gapminder_melt.columns = ['country','year','life_expectancy']

# Print the head of gapminder_melt
print(gapminder_melt.head())

# Assert that country does not contain any missing values
assert pd.notnull(gapminder.country).all()

# Assert that year does not contain any missing values
assert pd.notnull(gapminder.year).all()

# Drop the missing values
gapminder = gapminder.dropna()

# Print the shape of gapminder
print(gapminder.shape)


# Add first subplot
plt.subplot(2, 1, 1) 

# Create a histogram of life_expectancy
gapminder['life_expectancy'].plot(kind='hist')

# Group gapminder by 'year' and aggregate 'life_expectancy' by the mean. To do this:
#
#    Use the .groupby() method on gapminder with 'year' as the argument. Then select 'life_expectancy' # and chain the .mean() method to it.
# Group gapminder: gapminder_agg
gapminder_agg = gapminder.groupby('year')['life_expectancy'].mean()

# Print the head of gapminder_agg
print(gapminder_agg.head())

# Print the tail of gapminder_agg
print(gapminder_agg.tail())

# Add second subplot
plt.subplot(2, 1, 2)

# Create a line plot of life expectancy per year
gapminder_agg.plot()

# Add title and specify axis labels
plt.title('Life expectancy over the years')
plt.ylabel('Life expectancy')
plt.xlabel('Year')

# Display the plots
plt.tight_layout()
plt.show()

# Save both DataFrames to csv files
gapminder.to_csv('gapminder1.csv')

gapminder_agg.to_csv('gapminder_agg1.csv')

print(":")