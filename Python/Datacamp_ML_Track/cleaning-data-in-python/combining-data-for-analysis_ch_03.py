import pandas as pd
import matplotlib.pyplot as plt
import numpy as np  
import glob

# Q 1

# Import pandas

# Read the file into a DataFrame: df

df = pd.read_csv('nyc_uber_2014.csv')
print(df)
col_name = 'Unnamed: 0'
uber1 = pd.DataFrame(df[0 :  99])
uber2 = pd.DataFrame(df[99 :  198])
uber3 = pd.DataFrame(df[198 : ])

# reassign indices and drop old ones otherwise reaasigned to anew column
mykeys = np.arange(0,99)

uber2.set_index(keys=mykeys, drop=True, append=False, inplace=True, verify_integrity=False)
uber3.set_index(keys=mykeys, drop=True, append=False, inplace=True, verify_integrity=False)

uber1.drop(columns=col_name, axis=1,  inplace=True)
uber2.drop(columns=col_name, axis=1,  inplace=True)
uber3.drop(columns=col_name, axis=1,  inplace=True)


# Here starts data camp shit after 
# Concatenate uber1, uber2, and uber3: row_concat
row_concat = pd.concat([uber1,uber2,uber3])

# Print the shape of row_concat
print(row_concat.shape)

# Print the head of row_concat
print(row_concat.head())


ebola = pd.read_csv("ebola.csv")
ebola_melt = pd.melt(ebola, id_vars=['Date', 'Day'], var_name='type_country', value_name='counts')
print(ebola_melt.head())

# Create the 'str_split' column
df_split = ebola_melt.type_country.str.split("_")

print(df_split.head())
status_country = pd.DataFrame()
# Create the 'type' column
status_country['type'] = df_split.iloc[:].str.get(0)
# Create the 'country' column
status_country['country'] = df_split.iloc[:].str.get(1)

# Concatenate ebola_melt and status_country column-wise: ebola_tidy
ebola_tidy = pd.concat([ebola_melt,status_country],axis=1)

# Print the shape of ebola_tidy
print(ebola_tidy.shape)

# Print the head of ebola_tidy
print(ebola_tidy.head())

# Write the pattern: pattern
pattern = '*.csv'

# Save all file matches: csv_files
csv_files = glob.glob(pattern)

# Print the file names
print(csv_files)

# Load the second file into a DataFrame: csv2
csv2 = pd.read_csv(csv_files[1])

# Print the head of csv2
print(csv2.head())


# Create an empty list: frames
frames = []

#  Iterate over csv_files
for csv in csv_files:

    #  Read csv into a DataFrame: df
    df = pd.read_csv(csv)
    
    # Append df to frames
    frames.append(df)

# Concatenate frames into a single DataFrame: uber
uber = pd.concat(frames)

# Print the shape of uber
print(uber.shape)


# mergeing data: equivalent to sql join: merging on simlar keys
# like the example
# one-to-one , one-to-many, many-to-many type of merging 
# Not possible requires sql db
# Merge the DataFrames: o2o
#o2o = pd.merge(left=site, right=visited, left_on='name', right_on='site')
# Print o2o
#print(o2o)

df1 = pd.DataFrame({"c1":['a', 'a', 'b','b'], "c2":[1, 2, 3, 4]})
df2 = pd.DataFrame({"c1":['a', 'a', 'b','b'], "c2":[10, 20, 30, 40]})
df3_man = pd.DataFrame({"c1":['a', 'a','a', 'a', 'b','b','b','b'], "c2_x":[1, 1, 2, 2,3, 3, 4, 4],"c2_y":[10,10,20, 20, 30,30,40, 40]})
#df3 = pd.merge(left=df1,right=df2,left_on='c1',right_on='c2')                   

print(df3_man)
## Merge site and visited: m2m (many to many )
#m2m = pd.merge(left=site, right=visited, left_on='name', right_on='site')
## Merge m2m and survey: m2m
#m2m = pd.merge(left=m2m, right=survey, left_on='ident', right_on='taken')
#print(m2m.head(20))





print(":")