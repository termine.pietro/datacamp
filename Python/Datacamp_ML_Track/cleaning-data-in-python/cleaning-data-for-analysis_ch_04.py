import pandas as pd
import matplotlib.pyplot as plt
import numpy as np  
import glob
import re

tips = pd.read_csv('tips.csv')
# Convert the sex column to type 'category'
tips.sex = tips.sex.astype('category')

# Convert the smoker column to type 'category'
tips.smoker = tips.smoker.astype('category')

# Print the info of tips
print(tips.info())

# Convert 'total_bill' to a numeric dtype
tips['total_bill'] = pd.to_numeric(tips['total_bill'], errors='coerse')

# Convert 'tip' to a numeric dtype
tips['tip'] = pd.to_numeric(tips['tip'],errors='coerce')

# Print the info of tips
print(tips.info())

# using regex for data cleaning 
# Import the regular expression module

# Compile a pattern that matches a phone number of the format xxx-xxx-xxxx.
# Use \d{x} to match x digits. Here you'll need to use it three times: twice to match 3 digits, and once #to match 4 digits.
# Compile the pattern: prog
prog = re.compile('\d{3}-\d{3}-\d{4}')

# See if the pattern matches
result = prog.match('123-456-7890')
print(bool(result))

# See if the pattern matches
result2 = prog.match('1123-456-7890')
print(bool(result2))

#Write a pattern that will find all the numbers in the following string: 'the recipe calls for 10 #strawberries and 1 banana'. To do this:

#    Use the re.findall() function and pass it two arguments: the pattern, followed by the string.
#    \d is the pattern required to find digits. This should be followed with a + so that the previous #element is matched one or more times. This ensures that 10 is viewed as one number and not as 1 and 0.

# Find the numeric values: matches
matches = re.findall('\d+', 'the recipe calls for 10 strawberries and 1 banana')

# Print the matches
print(matches)

#Write patterns to match:

#    A telephone number of the format xxx-xxx-xxxx. You already did this in a previous exercise.
#    A string of the format: A dollar sign, an arbitrary number of digits, a decimal point, 2 digits.
#        Use \$ to match the dollar sign, \d* to match an arbitrary number of digits, \. to match the #decimal point, and \d{x} to match x number of digits.
#    A capital letter, followed by an arbitrary number of alphanumeric characters.
#        Use [A-Z] to match any capital letter followed by \w* to match an arbitrary number of #alphanumeric characters.

# Write the first pattern
pattern1 = bool(re.match(pattern='\d{3}-\d{3}-\d{4}', string='123-456-7890'))
print(pattern1)

# Write the second pattern
pattern2 = bool(re.match(pattern='^\$\d*.\d{2}', string='$123.45'))
print(pattern2)

# Write the third pattern
pattern3 = bool(re.match(pattern='^\[A-Z]\w*', string='Australia'))
print(pattern3)


tips = pd.read_csv('tips.csv')

# Define recode_gender()
def recode_gender(gender):

    # Return 0 if gender is 'Female'
    if gender == 'Female':
        return 0
    
    # Return 1 if gender is 'Male'    
    elif gender == 'Male':
        return 1
    
    # Return np.nan    
    else:
        return np.nan

# Apply the function to the sex column
tips['recode'] = tips.sex.apply(recode_gender) # by default apply works on row axis = 1 t make it works on the whole column

# Print the first five rows of tips
print(tips.head())

# Write the lambda function using replace
#tips['total_dollar_replace'] = tips.total_dollar.apply(lambda x: x.replace('$', ''))

## Write the lambda function using regular expressions
#tips['total_dollar_re'] = tips.total_dollar.apply(lambda x: re.findall('\d+\.\d+', x)[0])

# Print the head of tips
print(tips.head())

#not avaible billboard = 

# Create the new DataFrame: tracks
#tracks = billboard[['year','artist','track','time']]

# Print info of tracks
#print(tracks.info())

# Drop the duplicates: tracks_no_duplicates
#tracks_no_duplicates = tracks.drop_duplicates()

# Print info of tracks
#print(tracks_no_duplicates.info())

airquality = pd.read_csv('airquality.csv')

# Calculate the mean of the Ozone column: oz_mean
oz_mean = airquality['Ozone'].mean()

# Replace all the missing values in the Ozone column with the mean
airquality['Ozone'] = airquality['Ozone'].fillna(oz_mean)

# Print the info of airquality
print(airquality.info())
print(airquality['Ozone'])

# asserting 



print(":")