from scipy.optimize import minimize
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn import datasets
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split 
from sklearn.model_selection import GridSearchCV

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from datasets_and_functions import *
# We'll use SVC instead of LinearSVC from now on
from sklearn.svm import SVC

def show_digit(i, lr=None):
    plt.imshow(np.reshape(X[i], (8,8)), cmap='gray', vmin = 0, vmax = 16, interpolation=None)
    plt.xticks(())
    plt.yticks(())
    if lr is None:
        plt.title("class label = %d" % y[i])
    else:
        pred = lr.predict(X[i][None])
        pred_prob = lr.predict_proba(X[i][None])[0,pred]
        plt.title("label=%d, prediction=%d, proba=%.2f" % (y[i], pred, pred_prob))
    plt.show()

seed = 12234
digits = datasets.load_digits()
X = digits.data
y = digits.target
X_train, X_test, y_train, y_test = train_test_split(digits.data, digits.target,test_size=0.25,random_state = seed)

lr = LogisticRegression()
lr.fit(X,y)

# Get predicted probabilities
proba = lr.predict_proba(X)

# Sort the example indices by their maximum probability
proba_inds = np.argsort(np.max(proba,axis=1)) # acending order less to most

# Show the most confident (least ambiguous) digit
show_digit(proba_inds[-1], lr)

# Show the least confident (most ambiguous) digit
show_digit(proba_inds[0], lr)

# Fit one-vs-rest logistic regression classifier
lr_ovr = LogisticRegression() # one-vs-rest 
lr_ovr.fit(X_train, y_train)

print("OVR training accuracy:", lr_ovr.score(X_train, y_train))
print("OVR test accuracy    :", lr_ovr.score(X_test, y_test))

# Fit softmax classifier
lr_mn = LogisticRegression(multi_class="multinomial",solver="lbfgs")
lr_mn.fit(X_train, y_train)

print("Softmax training accuracy:", lr_mn.score(X_train, y_train))
print("Softmax test accuracy    :", lr_mn.score(X_test, y_test))
print(lr_ovr.coef_.shape)
print(lr_ovr.intercept_.shape)
print(lr_mn.coef_.shape)
print(lr_mn.intercept_.shape)

lr_ovr = LogisticRegression(C=100) # one-vs-rest 
lr_ovr.fit(X_train_1, y_train_1)
lr_mn = LogisticRegression(multi_class="multinomial",solver="lbfgs",C=100)
lr_mn.fit(X_train_1, y_train_1)

# Print training accuracies
print("Softmax     training accuracy:", lr_mn.score(X_train_1, y_train_1))
print("One-vs-rest training accuracy:", lr_ovr.score(X_train_1, y_train_1))

plot_classifier(X_train_1, y_train_1, lr_ovr, proba=False)
plot_classifier(X_train_1, y_train_1, lr_mn, proba=False)

# Create the binary classifier (class 1 vs. rest) #training a single one label same thing is done automatically by one vs all 
lr_class_1 = LogisticRegression(C=100)
lr_class_1.fit(X_train_1, y_train_1==1)

# Plot the binary classifier (class 1 vs. rest)
plot_classifier(X_train_1, y_train_1==1, lr_class_1)



# Create/plot the binary classifier (class 1 vs. rest)
svm_class_1 = SVC()
svm_class_1.fit(X_train_1,y_train_1==1)
plot_classifier(X_train_1,y_train_1==1,svm_class_1) 