
import pandas as pd

export = 0
if export == 1:
    out_txt = []
    with open('./data.txt','r') as f:
        out_txt = f.read()

    out_lines = out_txt.split('\\n')

    print(out_lines)
    with open('./TrainingData.csv','w+') as f:
        for l in out_lines:
            f.write(l+'\n')


df = pd.read_csv("TrainingData.csv",index_col=0)
df[["FTE", "Total"]] = df[["FTE", "Total"]].apply(pd.to_numeric)
print(df.head())
print(df.info())
