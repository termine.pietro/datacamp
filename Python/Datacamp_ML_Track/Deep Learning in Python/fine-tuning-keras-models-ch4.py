import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from keras.layers import Dense
from keras.models import Sequential
from keras.utils import to_categorical
from keras.optimizers import SGD 
from keras.callbacks import EarlyStopping

def get_new_model():
    model = Sequential()
    model.add(Dense(50, activation='relu', input_shape=(n_cols,)))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(2,activation='softmax'))
    return model


df = pd.read_csv('titanic_all_numeric.csv')

predictors = df.drop(columns=['survived'])
predictors = predictors.values
# Convert the target to categorical: target
target = to_categorical(df.survived)
n_cols = predictors.shape[1]


pred_data = np.array([[2, 34.0, 0, 0, 13.0, 1, False, 0, 0, 1],
                      [2, 31.0, 1, 1, 26.25, 0, False, 0, 0, 1],
                      [1, 11.0, 1, 2, 120.0, 1, False, 0, 0, 1],
                      [3, 0.42, 0, 1, 8.5167, 1, False, 1, 0, 0],
                      [3, 27.0, 0, 0, 6.975, 1, False, 0, 0, 1],
                      [3, 31.0, 0, 0, 7.775, 1, False, 0, 0, 1],
                      [1, 39.0, 0, 0, 0.0, 1, False, 0, 0, 1],
                      [3, 18.0, 0, 0, 7.775, 0, False, 0, 0, 1],
                      [2, 39.0, 0, 0, 13.0, 1, False, 0, 0, 1],
                      [1, 33.0, 1, 0, 53.1, 0, False, 0, 0, 1],
                      [3, 26.0, 0, 0, 7.8875, 1, False, 0, 0, 1],
                      [3, 39.0, 0, 0, 24.15, 1, False, 0, 0, 1],
                      [2, 35.0, 0, 0, 10.5, 1, False, 0, 0, 1],
                      [3, 6.0, 4, 2, 31.275, 0, False, 0, 0, 1],
                      [3, 30.5, 0, 0, 8.05, 1, False, 0, 0, 1],
                      [1, 29.69911764705882, 0, 0, 0.0, 1, True, 0, 0, 1],
                      [3, 23.0, 0, 0, 7.925, 0, False, 0, 0, 1],
                      [2, 31.0, 1, 1, 37.0042, 1, False, 1, 0, 0],
                      [3, 43.0, 0, 0, 6.45, 1, False, 0, 0, 1],
                      [3, 10.0, 3, 2, 27.9, 1, False, 0, 0, 1],
                      [1, 52.0, 1, 1, 93.5, 0, False, 0, 0, 1],
                      [3, 27.0, 0, 0, 8.6625, 1, False, 0, 0, 1],
                      [1, 38.0, 0, 0, 0.0, 1, False, 0, 0, 1],
                      [3, 27.0, 0, 1, 12.475, 0, False, 0, 0, 1],
                      [3, 2.0, 4, 1, 39.6875, 1, False, 0, 0, 1],
                      [3, 29.69911764705882, 0, 0, 6.95, 1, True, 0, 1, 0],
                      [3, 29.69911764705882, 0, 0, 56.4958, 1, True, 0, 0, 1],
                      [2, 1.0, 0, 2, 37.0042, 1, False, 1, 0, 0],
                      [3, 29.69911764705882, 0, 0, 7.75, 1, True, 0, 1, 0],
                      [1, 62.0, 0, 0, 80.0, 0, False, 0, 0, 0],
                      [3, 15.0, 1, 0, 14.4542, 0, False, 1, 0, 0],
                      [2, 0.83, 1, 1, 18.75, 1, False, 0, 0, 1],
                      [3, 29.69911764705882, 0, 0, 7.2292, 1, True, 1, 0, 0],
                      [3, 23.0, 0, 0, 7.8542, 1, False, 0, 0, 1],
                      [3, 18.0, 0, 0, 8.3, 1, False, 0, 0, 1],
                      [1, 39.0, 1, 1, 83.1583, 0, False, 1, 0, 0],
                      [3, 21.0, 0, 0, 8.6625, 1, False, 0, 0, 1],
                      [3, 29.69911764705882, 0, 0, 8.05, 1, True, 0, 0, 1],
                      [3, 32.0, 0, 0, 56.4958, 1, False, 0, 0, 1],
                      [1, 29.69911764705882, 0, 0, 29.7, 1, True, 1, 0, 0],
                      [3, 20.0, 0, 0, 7.925, 1, False, 0, 0, 1],
                      [2, 16.0, 0, 0, 10.5, 1, False, 0, 0, 1],
                      [1, 30.0, 0, 0, 31.0, 0, False, 1, 0, 0],
                      [3, 34.5, 0, 0, 6.4375, 1, False, 1, 0, 0],
                      [3, 17.0, 0, 0, 8.6625, 1, False, 0, 0, 1],
                      [3, 42.0, 0, 0, 7.55, 1, False, 0, 0, 1],
                      [3, 29.69911764705882, 8, 2, 69.55, 1, True, 0, 0, 1],
                      [3, 35.0, 0, 0, 7.8958, 1, False, 1, 0, 0],
                      [2, 28.0, 0, 1, 33.0, 1, False, 0, 0, 1],
                      [1, 29.69911764705882, 1, 0, 89.1042, 0, True, 1, 0, 0],
                      [3, 4.0, 4, 2, 31.275, 1, False, 0, 0, 1],
                      [3, 74.0, 0, 0, 7.775, 1, False, 0, 0, 1],
                      [3, 9.0, 1, 1, 15.2458, 0, False, 1, 0, 0],
                      [1, 16.0, 0, 1, 39.4, 0, False, 0, 0, 1],
                      [2, 44.0, 1, 0, 26.0, 0, False, 0, 0, 1],
                      [3, 18.0, 0, 1, 9.35, 0, False, 0, 0, 1],
                      [1, 45.0, 1, 1, 164.8667, 0, False, 0, 0, 1],
                      [1, 51.0, 0, 0, 26.55, 1, False, 0, 0, 1],
                      [3, 24.0, 0, 3, 19.2583, 0, False, 1, 0, 0],
                      [3, 29.69911764705882, 0, 0, 7.2292, 1, True, 1, 0, 0],
                      [3, 41.0, 2, 0, 14.1083, 1, False, 0, 0, 1],
                      [2, 21.0, 1, 0, 11.5, 1, False, 0, 0, 1],
                      [1, 48.0, 0, 0, 25.9292, 0, False, 0, 0, 1],
                      [3, 29.69911764705882, 8, 2, 69.55, 0, True, 0, 0, 1],
                      [2, 24.0, 0, 0, 13.0, 1, False, 0, 0, 1],
                      [2, 42.0, 0, 0, 13.0, 0, False, 0, 0, 1],
                      [2, 27.0, 1, 0, 13.8583, 0, False, 1, 0, 0],
                      [1, 31.0, 0, 0, 50.4958, 1, False, 0, 0, 1],
                      [3, 29.69911764705882, 0, 0, 9.5, 1, True, 0, 0, 1],
                      [3, 4.0, 1, 1, 11.1333, 1, False, 0, 0, 1],
                      [3, 26.0, 0, 0, 7.8958, 1, False, 0, 0, 1],
                      [1, 47.0, 1, 1, 52.5542, 0, False, 0, 0, 1],
                      [1, 33.0, 0, 0, 5.0, 1, False, 0, 0, 1],
                      [3, 47.0, 0, 0, 9.0, 1, False, 0, 0, 1],
                      [2, 28.0, 1, 0, 24.0, 0, False, 1, 0, 0],
                      [3, 15.0, 0, 0, 7.225, 0, False, 1, 0, 0],
                      [3, 20.0, 0, 0, 9.8458, 1, False, 0, 0, 1],
                      [3, 19.0, 0, 0, 7.8958, 1, False, 0, 0, 1],
                      [3, 29.69911764705882, 0, 0, 7.8958, 1, True, 0, 0, 1],
                      [1, 56.0, 0, 1, 83.1583, 0, False, 1, 0, 0],
                      [2, 25.0, 0, 1, 26.0, 0, False, 0, 0, 1],
                      [3, 33.0, 0, 0, 7.8958, 1, False, 0, 0, 1],
                      [3, 22.0, 0, 0, 10.5167, 0, False, 0, 0, 1],
                      [2, 28.0, 0, 0, 10.5, 1, False, 0, 0, 1],
                      [3, 25.0, 0, 0, 7.05, 1, False, 0, 0, 1],
                      [3, 39.0, 0, 5, 29.125, 0, False, 0, 1, 0],
                      [2, 27.0, 0, 0, 13.0, 1, False, 0, 0, 1],
                      [1, 19.0, 0, 0, 30.0, 0, False, 0, 0, 1],
                      [3, 29.69911764705882, 1, 2, 23.45, 0, True, 0, 0, 1],
                      [1, 26.0, 0, 0, 30.0, 1, False, 1, 0, 0],
                      [3, 32.0, 0, 0, 7.75, 1, False, 0, 1, 0]])


# Create list of learning rates: lr_to_test
lr_to_test = [.000001, 0.01, 1]

# Loop over learning rates
for lr in lr_to_test:
    print('\n\nTesting model with learning rate: %f\n'%lr )
    
    # Build new model to test, unaffected by previous models
    model = get_new_model()
    
    # Create SGD optimizer with specified learning rate: my_optimizer
    my_optimizer = SGD(lr=lr)
    
    # Compile the model
    model.compile(optimizer=my_optimizer,loss='categorical_crossentropy')
    
    # Fit the model
    model.fit(predictors,target)


# Save the number of columns in predictors: n_cols
n_cols = predictors.shape[1]
input_shape = (n_cols,)

# Specify the model
model = Sequential()
model.add(Dense(100, activation='relu', input_shape = input_shape))
model.add(Dense(100, activation='relu'))
model.add(Dense(2, activation='softmax'))

# Compile the model
model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])

# Fit the model
hist = model.fit(predictors,target,validation_split=0.3)


# Save the number of columns in predictors: n_cols
n_cols = predictors.shape[1]
input_shape = (n_cols,)

# Specify the model
model = Sequential()
model.add(Dense(100, activation='relu', input_shape = input_shape))
model.add(Dense(100, activation='relu'))
model.add(Dense(2, activation='softmax'))

# Compile the model
model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])

# Define early_stopping_monitor
early_stopping_monitor = EarlyStopping(patience=2)
model.fit(predictors,target,validation_split=0.3,callbacks=[early_stopping_monitor],epochs=30)

# A model called model_1 has been pre-loaded. You can see a summary of this model printed in the IPython Shell. 
# This is a relatively small network, with only 10 units in each hidden layer. 
# Create the new model: model_2
model_2 = Sequential()
model_1 = Sequential()
model_1.add(Dense(10,activation='relu', input_shape=input_shape))
model_1.add(Dense(10,activation='relu'))
# Add the first and second layers
model_2.add(Dense(100,activation='relu', input_shape=input_shape))
model_2.add(Dense(100,activation='relu'))

# Add the output layer
model_1.add(Dense(2,activation='softmax'))
model_2.add(Dense(2,activation='softmax'))

# Compile model_2
model_1.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
model_2.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])

# Fit model_1
model_1_training = model_1.fit(predictors, target, epochs=15, validation_split=0.2, callbacks=[early_stopping_monitor], verbose=False)

# Fit model_2
model_2_training = model_2.fit(predictors, target, epochs=15, validation_split=0.2, callbacks=[early_stopping_monitor], verbose=False)

# Create the plot
plt.plot(model_1_training.history['val_loss'], 'r', model_2_training.history['val_loss'], 'b')
plt.xlabel('Epochs')
plt.ylabel('Validation score')
plt.show()



# Once again, you have a baseline model called model_1 as a starting point. It has 1 hidden layer, with 50 units. 
# You can see a summary of that model's structure printed out. You will create a similar network with 3 hidden layers 
# (still keeping 50 units in each layer).This will again take a moment to fit both models, 
# so you'll need to wait a few seconds to see the results after you run your code.

# The input shape to use in the first hidden layer
input_shape = (n_cols,)

# Create the new model: model_2
model_1 = Sequential()
model_2 = Sequential()

# Add the first, second, and third hidden layers
model_1.add(Dense(50,activation='relu',input_shape=(n_cols,)))

model_2.add(Dense(50,activation='relu',input_shape=(n_cols,)))
model_2.add(Dense(50,activation='relu'))
model_2.add(Dense(50,activation='relu'))

# Add the output layer
model_1.add(Dense(2,activation='softmax'))
model_2.add(Dense(2,activation='softmax'))

# Compile model_2
model_1.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
model_2.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])

# Fit model 1
model_1_training = model_1.fit(predictors, target, epochs=20, validation_split=0.4, callbacks=[early_stopping_monitor], verbose=False)

# Fit model 2
model_2_training = model_2.fit(predictors, target, epochs=20, validation_split=0.4, callbacks=[early_stopping_monitor], verbose=False)

# Create the plot
plt.plot(model_1_training.history['val_loss'], 'r', model_2_training.history['val_loss'], 'b')
plt.xlabel('Epochs')
plt.ylabel('Validation score')
plt.show()



df_mnist = np.loadtxt('mnist.csv', delimiter=',')

X = df_mnist[:,1 :]
y = to_categorical(df_mnist[:,0])

print(X)
print(y)
# Create the model: model
model = Sequential()

# Add the first hidden layer
model.add(Dense(100,activation='relu',input_shape=(784,))) # flatten 28x28 image 

# Add the second hidden layer
model.add(Dense(100,activation='relu'))

# Add the output layer
model.add(Dense(10,activation='softmax'))

# Compile the model
model.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])

# Fit the model
model.fit(X,y,validation_split=0.2,epochs=15)



print("")