
import numpy as np 
import matplotlib.pyplot as plt 
from sklearn.metrics import mean_squared_error

# rectified li near actovation f(x) = 0 x<=0 f(x) = x x >=0  
def relu(input):
    '''Define your relu activation function here'''
    # Calculate the value for the output of the relu function: output
    output = max(0, input)
    
    # Return the value just calculated
    return(output)

# Define predict_with_network()
def predict_with_network(input_data_row, weights):

    # Calculate node 0 value
    node_0_input = (input_data_row * weights['node_0']).sum()
    node_0_output = relu(node_0_input)

    # Calculate node 1 value
    node_1_input = (input_data_row * weights['node_1']).sum()
    node_1_output = relu(node_1_input)

    # Put node values into array: hidden_layer_outputs
    hidden_layer_outputs = np.array([node_0_output, node_1_output])
    
    # Calculate model output
    input_to_final_layer = (hidden_layer_outputs * weights['output']).sum()
    model_output = relu(input_to_final_layer)
    
    # Return model output
    return(model_output)

# The data point you will make a prediction for
input_data = np.array([0, 3])

# Sample weights
weights_0 = {'node_0': [2, 1],
             'node_1': [1, 2],
             'output': [1, 1]
            }

# The actual target value, used to calculate the error
target_actual = 3

# Make prediction using original weights
model_output_0 = predict_with_network(input_data, weights_0)

# Calculate error: error_0
error_0 = model_output_0 - target_actual

# Create weights that cause the network to make perfect prediction (3): weights_1
weights_1 = {'node_0': [2, 1],
             'node_1': [1, 2],
             'output': [1, 0]
            }

# Make prediction using new weights: model_output_1
model_output_1 = predict_with_network(input_data, weights_1)

# Calculate error: error_1
error_1 = model_output_1 - target_actual

# Print error_0 and error_1
print(error_0)
print(error_1)

weights_0 = {'node_0': np.array([2, 1]), 'node_1': np.array([1, 2]), 'output': np.array([1, 1])}
weights_1 = {'node_0': np.array([2, 1]), 'node_1': np.array([1. , 1.5]),'output': np.array([1. , 1.5])}
input_data = [np.array([0, 3]), np.array([1, 2]), np.array([-1, -2]), np.array([4, 0])]
target_actuals = [1, 3, 5, 7]   


# Create model_output_0 
model_output_0 = []
# Create model_output_1
model_output_1 = []

# Loop over input_data
for row in input_data:
    # Append prediction to model_output_0
    model_output_0.append(predict_with_network(row,weights_0))
    
    # Append prediction to model_output_1
    model_output_1.append(predict_with_network(row,weights_1))

# Calculate the mean squared error for model_output_0: mse_0
mse_0 = mean_squared_error(target_actuals,model_output_0)

# Calculate the mean squared error for model_output_1: mse_1
mse_1 = mean_squared_error(target_actuals,model_output_1)

# Print mse_0 and mse_1
print("Mean squared error with weights_0: %f" %mse_0)
print("Mean squared error with weights_1: %f" %mse_1)

# Calculating slopes
# 
# You're now going to practice calculating slopes. When plotting the mean-squared error loss function against predictions, 
# the slope is 2 * x * (y-xb), or 2 * input_data * error. Note that x and b may have multiple numbers (x is a vector for each data point, and b is a vector). 
# In this case, the output will also be a vector, which is exactly what you want.
# You're ready to write the code to calculate this slope while using a single data point. 
# You'll use pre-defined weights called weights as well as data for a single point called input_data. 
# The actual value of the target you want to predict is stored in target.

weights = np.array([0, 2, 1])
target = 0
input_data = np.array([1, 2, 3])


# Calculate the predictions: preds
preds = (weights * input_data).sum()

# Calculate the error: error
error = target - preds

# Calculate the slope: slope
slope = 2 * error * input_data 

# Print the slope
print(slope)

# Set the learning rate: learning_rate
learning_rate = 0.01

# Calculate the predictions: preds
preds = (weights * input_data).sum()

# Calculate the error: error
error = preds - target

# Calculate the slope: slope
slope = 2 * input_data * error

# Update the weights: weights_updated
weights_updated = weights - learning_rate * slope

# Get updated predictions: preds_updated
preds_updated = (weights_updated * input_data).sum()

# Calculate updated error: error_updated
error_updated = preds_updated - target

# Print the original error
print(error)
print(error_updated)


def get_slope(input_data, target,weights):
    preds = (input_data * weights).sum()
    return 2 * input_data * (preds - target)

def get_mse(input_data, target,weights):
    preds = (input_data * weights).sum()
    diff = (preds - target)
    return diff * diff
    
n_updates = 20
mse_hist = []
learning_rate = 0.01

alphas = np.arange(0.01,0.1,0.01)

for learning_rate in alphas:
# Iterate over the number of updates
    for i in range(n_updates):
        # Calculate the slope: slope
        slope = get_slope(input_data=input_data, target=target,weights=weights)
    
        # Update the weights: weights
        weights = weights - slope * learning_rate
    
        # Calculate mse with new weights: mse
        mse = get_mse(input_data=input_data, target=target,weights=weights)
    
        # Append the mse to mse_hist
        mse_hist.append(mse)

    # Plot the mse history
    plt.plot(mse_hist)
    plt.xlabel('Iterations')
    plt.ylabel('Mean Squared Error')
    plt.show()