# I'm a code cell, click me, then run me!
256 * 60 * 24 # Children × minutes × hours

def greet(first_name, last_name):
    greeting = 'My name is ' + last_name + ', ' + first_name + ' ' + last_name + '!'
    return greeting

# Replace with your first and last name.
# That is, unless your name is already James Bond.
print(greet('James', 'Bond'))

# Importing the pandas module
import pandas as pd

# Reading in the global temperature data
global_temp = pd.read_csv('datasets/global_temperature.csv')
#print(global_temp)
# Take a look at the first datapoints
print(global_temp.head())

#%matplotlib inline

import matplotlib.pyplot as plt

# Plotting global temperature in degrees celsius by year
plt.plot(global_temp['year'], global_temp['degrees_celsius'])

# Adding some nice labels 
plt.xlabel('years') 
plt.ylabel('temperature') 

plt.show()

# Making a map using the folium module
import folium
phone_map = folium.Map()

# Top three smart phone companies by market share in 2016
companies = [
    {'loc': [37.4970,  127.0266], 'label': 'Samsung:20.5%'},
    {'loc': [37.3318, -122.0311], 'label': 'Apple:14.4%'},
    {'loc': [22.5431,  114.0579], 'label': 'Huawei:8.9%'}] 

# Adding markers to the map
for company in companies:
    marker = folium.Marker(location=company['loc'], popup=company['label'])
    marker.add_to(phone_map)

# The last object in the cell always gets shown in the notebook
print(phone_map)