# Importing pandas
import pandas as pd

# Importing matplotlib and setting aesthetics for plotting later.
import matplotlib.pyplot as plt
#%matplotlib inline
#%config InlineBackend.figure_format = 'svg' 
plt.style.use('fivethirtyeight')

# Reading in current data from coinmarketcap.com
current = pd.read_json("https://api.coinmarketcap.com/v1/ticker/")

# Printing out the first few lines
print(current.head())

# Reading datasets/coinmarketcap_06122017.csv into pandas
dec6 = pd.read_csv("datasets/coinmarketcap_06122017.csv")

# Selecting the 'id' and the 'market_cap_usd' columns
market_cap_raw = dec6.loc[:,['id','market_cap_usd']]

# Counting the number of values
print(market_cap_raw.count())

# Filtering out rows without a market capitalization
cap = market_cap_raw.query('market_cap_usd > 0')

# Counting the number of values again
print(cap.count())

#Declaring these now for later use in the plots
TOP_CAP_TITLE = 'Top 10 market capitalization'
TOP_CAP_YLABEL = '% of total cap'

# Selecting the first 10 rows and setting the index
cap10 = cap.iloc[0:10,:].set_index('id')
tot = cap.market_cap_usd.sum()
cap10 = cap10.assign(market_cap_perc= lambda x: x.market_cap_usd *100 /tot)
#fig, ax = plt.subplots()
#ax.Axis(ylabel="% of total cap",title="Top 10 market capitalization")
ax = cap10.plot.bar()
ax.title.set_text(TOP_CAP_TITLE)
ax.set_ylabel(TOP_CAP_YLABEL)
ax.set_title(TOP_CAP_TITLE)

#ylabel="% of total cap",title="Top 10 market capitalization"
print(cap10)

# Colors for the bar plot
COLORS = ['orange', 'green', 'orange', 'cyan', 'cyan', 'blue', 'silver', 'orange', 'red', 'green']

# Plotting market_cap_usd as before but adding the colors and scaling the y-axis  
ax =  cap10['market_cap_usd'].plot.bar()
ax.set_ylabel('USD')
ax.set_xlabel('')
ax.set_title(TOP_CAP_TITLE)
ax.set_yscale('log')
# Final touch! Removing the xlabel as it is not very informative
# ... YOUR CODE FOR TASK 5 ..
print('')
