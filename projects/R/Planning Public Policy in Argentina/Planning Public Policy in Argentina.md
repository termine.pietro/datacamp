
## 1. Provinces of Argentina
<p><img style="float: left;margin:5px 20px 5px 1px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Buenos_Aires_Puerto_Madero_19.jpg/1200px-Buenos_Aires_Puerto_Madero_19.jpg" title="Photo by Andrzej Otrębski CC BY-SA 4.0"></p>
<p>With almost 40 million inhabitants and a diverse geography that encompasses the Andes mountains, glacial lakes, and the Pampas grasslands, Argentina is the second largest country (by area) and has one of the largest economies in South America. It is politically organized as a federation of 23 provinces and an autonomous city, Buenos Aires.</p>
<p>We will analyze ten economic and social indicators collected for each province. Because these indicators are highly correlated, we will use Principal Component Analysis (PCA) to reduce redundancies and highlight patterns that are not apparent in the raw data. After visualizing the patterns, we will use k-means clustering to partition the provinces into groups with similar development levels. </p>
<p>These results can be used to plan public policy by helping allocate resources to develop infrastructure, education, and welfare programs.</p>


```R
# Load the tidyverse
# .... YOUR CODE FOR TASK 1 ....

# Read in the dataset
argentina <- ....

# Inspect the first rows of the dataset
# .... YOUR CODE FOR TASK 1 ....
# .... YOUR CODE FOR TASK 1 ....
```


```R
library(testthat) 
library(IRkernel.testthat)

soln_argentina <- readr::read_csv("datasets/argentina.csv")

run_tests({
    
    test_that("The correct package was loaded.", {
        expect_true("tidyverse" %in% .packages(), 
                    info = "Please make sure that the tidyverse package is loaded.")
    })
    
    test_that("Read in data correctly.", {
        
        expect_is(argentina, "tbl_df", info="Did you use read_csv for reading the data in?")
        
        expect_identical(colnames(argentina),
                         colnames(soln_argentina),
            info = "`argentina` has the wrong columns. Did you import datasets/argentina.csv?")
    })
    
    test_that("Read in data correctly.", {
        expect_equivalent(argentina, soln_argentina, 
            info = 'argentina should contain the data in "datasets/argentina.csv"')
    })
})
```

## 2. Most populous, richest provinces
<p>Argentina ranks third in South America in total population, but the population is unevenly distributed throughout the country. Sixty percent of the population resides in the Pampa region (Buenos Aires, La Pampa, Santa Fe, Entre Ríos and Córdoba) which only encompasses about 20% of the land area.</p>
<p><a href="https://en.wikipedia.org/wiki/Gross_domestic_product">GDP</a> is a measure of the size of a province's economy. To measure how rich or poor the inhabitants are, economists use <a href="https://en.wikipedia.org/wiki/List_of_countries_by_GDP_(nominal)_per_capita"><em>per capita GDP</em></a>, which is GDP divided by the province's population.</p>


```R
# Add gdp_per_capita column to argentina
argentina <- argentina %>% 
  ....(gdp_per_cap = .... / ....) 

# Find the four richest provinces
( rich_provinces  <- argentina %>% 
# .... YOUR CODE FOR TASK 2 ....
 
# Find the provinces with populations over 1 million
( bigger_pops <- argentina %>% 
# .... YOUR CODE FOR TASK 2 ....
```


```R
soln_argentina <- soln_argentina %>% 
  mutate(gdp_per_cap = gdp / pop) 

soln_rich_provinces  <- soln_argentina %>% 
  arrange(-gdp_per_cap) %>% 
  select(province, gdp_per_cap) %>% 
  top_n(4)

soln_bigger_pops <- soln_argentina %>% 
  arrange(-pop) %>%          
  select(province, pop) %>%  
  filter(pop > 1e6)


run_tests({
    
    test_that("bigger_pops has correct columns", {
      expect_identical(
          colnames(soln_bigger_pops),
          colnames(bigger_pops), 
        info = "bigger_pops has incorrect columns. Did you select `province` and `pop`?")
    })
    
    test_that("bigger_pops has correct number of rows", {
      expect_true(nrow(bigger_pops) == 9, 
        info = "bigger_pops should have 9 rows")
    })
    
    test_that("bigger_pops has correct values", {
      expect_equivalent(bigger_pops, 
                        soln_bigger_pops, 
        info = "bigger_pops should have the provinces with more than 1 million inhabitants")
    })
    
    test_that("bigger_pops has correct columns", {
      expect_identical(
          colnames(soln_rich_provinces),
          colnames(rich_provinces), 
        info = "rich_provinces has incorrect columns. Did you select `province` and `gdp_per_cap`?")
    })
    
    test_that("rich_provinces has correct number of rows", {
      expect_true(nrow(rich_provinces) == 4, 
        info = "rich_provinces should have 4 rows")
    })
    
    test_that("rich_provinces has be correct ", {
      expect_equivalent(rich_provinces, soln_rich_provinces, 
        info = "rich_provinces should have the top 4 provinces by gdp_per_cap")
    })
})
```

## 3. A matrix for PCA
<p>Principal Component Analysis (PCA) is an unsupervised learning technique that summarizes multivariate data by reducing redundancies (variables that are correlated). New variables (the principal components) are linear combinations of the original data that retain as much variation as possible. We would imagine that some aspects of economic and social data would be highly correlated, so let's see what pops out. But first, we need to do some data preparation.</p>
<p>R makes it easy to run a PCA with the <code>PCA()</code> function from the <code>FactoMineR</code> package. The first argument in <code>PCA()</code> is a data frame or matrix of the data where the rows are "individuals" (or in our case, provinces) and columns are numeric variables. To prepare for the analysis, we will remove the column of province names and build a matrix from the dataset.</p>


```R
# Select numeric columns and cast to matrix
argentina_matrix  <- ....  %>% 
  select_....(....) %>%  
  ....

# Print the first lines of the result
# .... YOUR CODE FOR TASK 3 ....
```


```R
soln_argentina_matrix <- soln_argentina %>% 
                           select_if(is.numeric) %>%  
                           as.matrix 

run_tests({
    
    test_that("argentina_matrix is a matrix", {
      expect_true(class(argentina_matrix) == "matrix", 
        info = "Did you use as.matrix to cast your data.frame to a matrix?")
    })

    test_that("argentina_matrix has the correct data", {
        expect_equal(argentina_matrix, soln_argentina_matrix,
                    info = "argentina_matrix does not have the correct data. ")
    })
})
```

## 4. Reducing dimensions
<p>PCA finds a lower dimensional representation of the data that keeps the maximum amount of variance. It's great for analyzing multivariate datasets, like this one, with multiple numerical columns that are highly correlated. Typically, the first few components preserve most of the information in the raw data, allowing us, to go from eleven dimensions (eleven original variables) down to two dimensions (two variables that are summaries of the original eleven).</p>
<p>To run PCA, we need to make sure all the variables are on similar scales. Otherwise, variables with large variance will be overrepresented. In <code>PCA()</code> setting <code>scale.unit = TRUE</code> ensures that variables are scaled to unit variance before crunching the numbers.</p>
<p>Feel free to explore the output! </p>


```R
# Load FactoMineR
# .... YOUR CODE FOR TASK 4 ....

# Apply PCA and print results
( ....  <- ....(...., scale.unit = TRUE) )
```


```R
soln_argentina_pca <- FactoMineR::PCA(soln_argentina_matrix, scale.unit = TRUE)

run_tests({
    
    test_that("The correct package was loaded.", {
        expect_true("FactoMineR" %in% .packages(), 
                    info = "Please make sure that the FactoMineR package is loaded.")
    })
    
    test_that("Variance is scaled", {

      expect_true("PCA" %in% class(argentina_pca),
        info = "Did you use PCA from FactoMineR?")
    })
    
    test_that("Variance is scaled", {
      skip("skip slow test")
      argentina_pca_wrong <- PCA(argentina_matrix, scale.unit = FALSE)

      expect_false(argentina_pca$eig[1,1]==argentina_pca_wrong$eig[1,1],
        info = "Did you set scale.units to TRUE?")
    })
    
    test_that("Results are correct", {
        
      expect_true(argentina_pca$eig[1,1]==soln_argentina_pca$eig[1,1],
        info = "Did you apply PCA to argentina_matrix?")
    })  
})
```

## 5. PCA: Variables & Components
<p>Now that we have the principal components, we can see how the original variables are correlated among themselves and how the original variables are correlated with the principal components. We will build a plot using the <code>factoextra</code> package to help us understand these relationships. A correlation circle plot (also known as a variable correlation plot) shows the relationship among all variables as they are plotted on the first two principal components (Dimension 1 and Dimension 2).</p>
<p>To understand the plot, note that:</p>
<ul>
<li>Positively correlated variables have similar vectors.</li>
<li>The vectors of negatively correlated variables are on opposite sides of the plot origin (opposite quadrants).</li>
<li>Each axis represents a principal component. Vectors pointing in the direction of the component are correlated with that component.</li>
<li>The percentage of the original variance explained by each component (dimension) is given in parentheses in the axes labels.</li>
</ul>


```R
# Load factoextra
# .... YOUR CODE FOR TASK 5 ....

# Set the size of plots in this notebook
options(repr.plot.width=7, repr.plot.height=5)

# Plot the original variables and the first 2 components and print the plot object.
( pca_var_plot <- .... )

# Sum the variance preserved by the first two components. Print the result.
( variance_first_two_pca <- ....$....[...., ....] + ....$....[...., ....] )
```


```R
student_plot <- pca_var_plot
soln_pca_var_plot <- fviz_pca_var(soln_argentina_pca)


run_tests({
    
    test_that("The correct package was loaded.", {
        expect_true("FactoMineR" %in% .packages(), 
                    info = "Please make sure that the FactoMineR package is loaded.")
    })
       
    test_that("pca_var_plot is correct", {
            
      expect_s3_class(student_plot, "ggplot")
         
      expect_identical(soln_pca_var_plot$data, 
                       student_plot$data,
        info = "Did you use fviz_pca_var with argentina_pca as argument?")
        
      expect_identical(
          
        deparse(student_plot$mapping$x),
        deparse(soln_pca_var_plot$mapping$x),
          info = 'The `x` aesthetic is incorrect. Did you change the default settings to fviz_pca_var?'
      )  
        
      expect_identical(
          
        deparse(student_plot$mapping$y),
        deparse(soln_pca_var_plot$mapping$y),
          info = 'The `y` aesthetic is incorrect. Did you change the default settings to fviz_pca_var?'
      )
    })
    
    
    test_that("variance_first_two_pca is the sum of the first 2 component's variance", {

      expect_equal(variance_first_two_pca, 63.5489736117608, tolerance=1e-4,
        info = "Did you add the variance of the first two components?")
    })
})
```

## 6. Plotting the components
<p>With the first two principal components representing almost 65% of the variance, most of the information we are interested in is summarized in these two components. From the variable correlation plot, we can see that population and GDP are highly correlated; illiteracy, poverty, no healthcare, school dropout, and deficient infrastructure are correlated; and GDP per capita and movie theaters per capita are correlated.</p>
<p>But how do these correlations map to the provinces? To dive into that question, let's plot the individual principal components for each province and look for clusters.</p>


```R
# Visualize Dim2 vs. Dim1
# .... YOUR CODE FOR TASK 6 ....
```


```R
student_plot <- last_plot()
soln_plot <- fviz_pca_ind(soln_argentina_pca, title = "Provinces - PCA") 

run_tests({
    
    test_that("Student plot is correct", {
        
      expect_equivalent(
        student_plot$data,
        soln_plot$data,
        info = "Did you use argentina_pca as input to fviz_pca_ind?"
      )
    })
    
    
    test_that("last plot has correct labels", {
        
      expect_equal(student_plot$labels$title, 
                   soln_plot$labels$title, 
        info = "Did you add the title 'Provinces - PCA?'")
        
      expect_equivalent(
        student_plot$labels,
        soln_plot$labels,
        info = "Did you use the default settings for fviz_pca_ind?")
    })
})
```

## 7. Cluster using K means
<p>It looks like one province stands out and the rest follow the gradient along the second dimension. Are there clusters we are not detecting? Let's use K-means clustering to see if there are patterns we are not detecting.</p>


```R
# Set seed to 1234 for reproducibility
# .... YOUR CODE FOR TASK 7 ....

# Create an intermediate data frame with pca_1 and pca_2
argentina_comps <- tibble(pca_1 = ....$....$....[ ,....],  
                          pca_2 = ....$....$....[ ,....])

# Cluster the observations using the first 2 components and print its contents
( argentina_km <- .... )
```


```R
soln_argentina_comps <- tibble(pca_1 = soln_argentina_pca$ind$coord[,1],  
                               pca_2 = soln_argentina_pca$ind$coord[,2])

run_tests({
    
    test_that("intermediate data frame is correct", {    
      expect_equal(argentina_comps, 
                   soln_argentina_comps, 
        info = "argentina_comps must have one row per province and only 2 columns, pca_1 and pca_2")
    })
    
    test_that("kmeans finds correct number of clusters", {
        
      expect_equal(nrow(argentina_km$centers), 4, 
        info = "Did you create 4 clusters?")
    })
})
```

## 8. Components with colors
<p>Now that we have cluster assignments for each province, we will plot the provinces according to their principal components coordinates, colored by the cluster.</p>


```R
# Convert assigned clusters to factor
# .... YOUR CODE FOR TASK 8 ....

# Plot individulas colored by cluster
fviz_pca_ind(argentina_pca, 
             title = ...., 
             habillage = ....) 
```


```R
student_plot <- last_plot()
soln_plot <- fviz_pca_ind(soln_argentina_pca, 
             title = "Clustered Provinces - PCA", 
             habillage=factor(argentina_km$cluster))

run_tests({
      
    test_that("clusters_as_factors is a factor", {
                
      expect_is(clusters_as_factor, "factor",
               info = "Did you convert clusters to a factor?")
                
    })
    
   
    test_that("last plot has correct labels", {
                
      expect_equal(student_plot$labels$x, 
                   soln_plot$labels$x,
        info = "Did you use the default labels for the x axis?")
        
    })
    
    test_that("last plot has correct labels", {
        
      expect_equal(student_plot$labels$y, 
                   soln_plot$labels$y,
        info = "Did you use the default labels for the y axis?")
                
    })
    
    test_that("last plot has correct labels", {
        
      expect_equal(student_plot$labels$title, 
                    soln_plot$labels$title,
        info = "Did you add the title Clustered Provinces - PCA?")
    })
    
    test_that("last plot has Groups column in data ", {
        
      expect_equal(student_plot$data$Groups, 
                   soln_plot$data$Groups,
        info = "Did you set the habillage argument when calling fviz_pca_ind?")
    })

})
```

## 9. Buenos Aires, in a league of its own
<p>A few things to note from the scatter plot:</p>
<ul>
<li>Cluster 1 includes only Buenos Aires and has a large positive value in Dimension 2 with an intermediate negative value in Dimension 1.</li>
<li>Cluster 2 has the greatest negative values in Dimension 1.</li>
<li>Cluster 3 has the greatest positive values in Dimension 1.</li>
<li>Cluster 4 has small absolute values in Dimension 1.</li>
<li>Clusters 2, 3, and 4, all have small absolute values in Dimension 2.</li>
</ul>
<p>We will focus on exploring clusters 1, 2, and 3 in terms of the original variables in the next few tasks.</p>
<p>As we noted earlier, Buenos Aires is in a league of its own, with the largest positive value in Dimension 2 by far. The figure below is a biplot, a combination of the individuals plot from Task 6 and the circle plot from Task 5.</p>
<p><img width="700px" height="700px" src="https://assets.datacamp.com/production/project_638/img/biplot.png"></p>
<p>Since the vectors corresponding to <code>gdp</code> and <code>pop</code> are in the same direction as Dimension 2, Buenos Aires has high GDP and high population. Let's visualize this pattern with a plot of <code>gdp</code> against <code>cluster</code> (we should get similar results with <code>pop</code>).</p>


```R
# Load ggrepel
# .... YOUR CODE FOR TASK 9 ....

# Add cluster column to argentina
argentina <- argentina %>%
               mutate(cluster=....)

# Make a scatterplot of gdp vs. cluster, colored by cluster
ggplot(argentina, aes(...., ...., .... = ....)) +
  # .... YOUR CODE FOR TASK 9 ....
  ....(aes(label = ....), show.legend = FALSE) +
  ....(x = "Cluster", y = "GDP")
```


```R
student_plot <- last_plot()

soln_argentina_km <- kmeans(soln_argentina_comps, centers = 4, nstart = 20, iter.max = 50) 

soln_argentina <- mutate(soln_argentina, 
                           cluster=factor(argentina_km$cluster))

# Make a scatterplot of gdp vs. cluster, colored by cluster
soln_plot <- ggplot(soln_argentina, 
                    aes(cluster, gdp, color=cluster)) +
             geom_point() +
             ggrepel::geom_text_repel(aes(label=province), show.legend=FALSE) +
             labs(x="Cluster", y="GDP")


run_tests({
    
    test_that("The correct package was loaded.", {
        expect_true("ggrepel" %in% .packages(), 
                    info = "Please make sure that the ggrepel package is loaded.")
    })
    
    
    test_that("added cluster factor", {
        
      expect_s3_class(argentina$cluster, "factor")
        
      expect_identical(
        colnames(argentina),
        colnames(soln_argentina),
        info = "`argentina` does not have the correct columns")
        
      expect_true(
        "cluster" %in% colnames(argentina),
        info = "Did you add a cluster column to `argentina`?"
      )
        
    })
      
    test_that("The plot is correct", {
        
      expect_s3_class(student_plot, "ggplot")
        
      expect_identical(
        soln_plot$data,
        student_plot$data,
          info = "Did you use `argentina` for your plot?"
      )
        
      expect_identical(
        deparse(student_plot$mapping$x),
        deparse(soln_plot$mapping$x),
        info = 'The `x` aesthetic is incorrect. Did you map it to `cluster`?'
      )
        
      expect_identical(
        deparse(student_plot$mapping$y),
        deparse(soln_plot$mapping$y),
        info = 'The `y` aesthetic is incorrect. Did you map it to `gdp`?'
      )
        
      expect_identical(
        deparse(student_plot$mapping$group),
        deparse(soln_plot$mapping$group),
        info = 'The `group` aesthetic is incorrect. Did you map it to `cluster`?'
      )
                
      expect_identical(
        student_plot$labels$x,
        soln_plot$labels$x,
        info = "The label in the x-axis is incorrect. Did you set it to `Cluster`?"
      )
      expect_identical(
        student_plot$labels$y,
        soln_plot$labels$y,
        info = "The label in the y-axis is incorrect. Did you set it to `GDP`?"
      )
    })
    
    # Testing the geoms with %in% 
    test_that("last plot has geom_point and geom_text_repel layers", {
        
      student_plot_layers <- student_plot$layers
      student_plot_layers_geoms <- map(student_plot_layers, ~class(.$geom)[[1]])

      expect_true("GeomPoint" %in% student_plot_layers_geoms,
        info = "Did you add a geom_point() layer to your plot?")
        
      expect_true("GeomTextRepel" %in% student_plot_layers_geoms,
        info = "Did you add a geom_text_repel() layer to your plot?")
    })
    
})
```

## 10. The rich provinces
<p>Provinces in cluster 2 have large negative values in Dimension 1. The biplot shows that <code>gdp_per_cap</code>, <code>movie_theaters_per_cap</code> and <code>doctors_per_cap</code> also have high negative values in Dimension 1. </p>
<p><img width="700px" height="700px" src="https://assets.datacamp.com/production/project_638/img/biplot.png"></p>
<p>If we plot <code>gdp_per_cap</code> for each cluster, we can see that provinces in this cluster 2, in general, have greater GDP per capita than the provinces in the other clusters. San Luis is the only province from the other clusters with <code>gdp_per_cap</code> in the range of values observed in cluster 2. We will see similar results for <code>movie_theaters_per_cap</code> and <code>doctors_per_cap</code>.</p>


```R
# Make a scatterplot of GDP per capita vs. cluster, colored by cluster
ggplot(argentina, aes(...., ...., .... = ....)) +
  # .... YOUR CODE FOR TASK 10 ....
  ....(aes(label = ....), show.legend = FALSE) +
  ....(x = "Cluster", y = "GDP per capita")
```


```R
student_plot <- last_plot()

soln_plot <- ggplot(soln_argentina, aes(cluster, gdp_per_cap, color=cluster)) +
  geom_point() + 
  geom_text_repel(aes(label=province), show.legend=FALSE) +
  labs(x="Cluster", y="GDP per capita")


run_tests({
    
      
    test_that("The plot is correct", {
        
      expect_s3_class(student_plot, "ggplot")
        
      expect_identical(
        soln_plot$data,
        student_plot$data,
          info = "Did you use `argentina` for your plot?"
      )
        
      expect_identical(
        deparse(student_plot$mapping$x),
        deparse(soln_plot$mapping$x),
        info = 'The `x` aesthetic is incorrect. Did you map it to `cluster`?'
      )
        
      expect_identical(
        deparse(student_plot$mapping$y),
        deparse(soln_plot$mapping$y),
        info = 'The `y` aesthetic is incorrect. Did you map it to `gdp_per_cap`?'
      )
        
      expect_identical(
        deparse(student_plot$mapping$group),
        deparse(soln_plot$mapping$group),
        info = 'The `group` aesthetic is incorrect. Did you map it to `cluster`?'
      )
                        
      expect_identical(
        student_plot$labels$x,
        soln_plot$labels$x,
        info = "The label in the x-axis is incorrect. Did you set it to `Cluster`?"
      )
      expect_identical(
        student_plot$labels$y,
        soln_plot$labels$y,
        info = "The label in the y-axis is incorrect. Did you set it to `GDP per capita`?"
      )
    })
    
    # Testing the geoms with %in% 
    test_that("last plot has geom_point and geom_text_repel layers", {
        
      student_plot_layers <- student_plot$layers
      student_plot_layers_geoms <- map(student_plot_layers, ~class(.$geom)[[1]])

      expect_true("GeomPoint" %in% student_plot_layers_geoms,
        info = "Did you add a geom_point() layer to your plot?")
        
      expect_true("GeomTextRepel" %in% student_plot_layers_geoms,
        info = "Did you add a geom_text_repel() layer to your plot?")
    })
    
})
```

## 11. The poor provinces
<p>Provinces in Cluster 3 have high positive values in Dimension 1. As shown in the biplot, provinces with high positive values in Dimension 1 have high values in poverty, deficient infrastructure, etc. These variables are also negatively correlated with <code>gdp_per_cap</code>, so these provinces have low values in this variable.</p>
<p><img width="700px" height="700px" src="https://assets.datacamp.com/production/project_638/img/biplot.png"></p>


```R
# Make scatterplot of poverty vs. cluster, colored by cluster
ggplot(argentina, aes(...., ...., .... = ....)) +
  # .... YOUR CODE FOR TASK 11 ....
  ....(x = "Cluster", y = "Poverty rate") +
  ....(aes(label = ....), show.legend = FALSE)
```


```R
student_plot <- last_plot()

soln_plot <- ggplot(soln_argentina, aes(cluster, poverty, color = cluster)) +
  geom_point() +
  geom_text_repel(aes(label=province), show.legend = FALSE) +
  labs(x="Cluster", y="Poverty rate")


run_tests({
    
      
    test_that("The plot is correct", {
        
      expect_s3_class(student_plot, "ggplot")
        
      expect_identical(
        soln_plot$data,
        student_plot$data,
          info = "Did you use `argentina` for your plot?"
      )
        
      expect_identical(
        deparse(student_plot$mapping$x),
        deparse(soln_plot$mapping$x),
        info = 'The `x` aesthetic is incorrect. Did you map it to `cluster`?'
      )
        
      expect_identical(
            deparse(student_plot$mapping$y),
            deparse(soln_plot$mapping$y),
            info = 'The `y` aesthetic is incorrect. Did you map it to `poverty`?'
      )
        
      expect_identical(
            deparse(student_plot$mapping$group),
            deparse(soln_plot$mapping$group),
            info = 'The `group` aesthetic is incorrect. Did you map it to `cluster`?'
      )
        
        
      # Should we test specific labels?
                        
      expect_identical(
            student_plot$labels$x,
            soln_plot$labels$x,
            info = "The label in the x-axis is incorrect. Did you set it to `Cluster`?"
      )
      expect_identical(
            student_plot$labels$y,
            soln_plot$labels$y,
            info = "The label in the y-axis is incorrect. Did you set it to `Poverty`?"
      )
    })
    
    # Testing the geoms with %in% 
    test_that("last plot has geom_point and geom_text_repel layers", {
        
      student_plot_layers <- student_plot$layers
      student_plot_layers_geoms <- map(student_plot_layers, ~class(.$geom)[[1]])

      expect_true("GeomPoint" %in% student_plot_layers_geoms,
        info = "Did you add a geom_point() layer to your plot?")
        
      expect_true("GeomTextRepel" %in% student_plot_layers_geoms,
        info = "Did you add a geom_text_repel() layer to your plot?")
    })
    
})
```

## 12. Planning for public policy
<p>Now that we have an idea of how social and economic welfare varies among provinces, we've been asked to help plan an education program. A pilot phase of the program will be carried out to identify design issues. Our goal is to select the proposal with the most diverse set of provinces:</p>
<ol>
<li>Tucumán, San Juán, and Entre Ríos</li>
<li>Córdoba, Santa Fé, and Mendoza</li>
<li>Buenos Aires, Santa Cruz, and Misiones </li>
</ol>
<p>Which proposal includes the most diverse set of provinces?</p>
<p><img width="800px" height="800px" src="https://assets.datacamp.com/production/project_638/img/components_color.png"></p>


```R
# Assign pilot provinces to the most diverse group
pilot_provinces <- ....
```


```R
run_tests({
    test_that("the answer is 1, 2 or 3", {
      expect_true(as.numeric(pilot_provinces) %in% c(1, 2, 3), 
        info = "You should choose one of groups 1, 2 or 3")
    })
    test_that("the answer is not 1", {
      expect_false(as.numeric(pilot_provinces) %in% c(1, 2), 
        info = "This set of provinces is on the same cluster, and are very similar")
    })
})
```
