-- Select average life_expectancy
SELECT AVG(life_expectancy)
  -- From populations
  FROM populations
-- Where year is 2015
WHERE year = 2015;


/*
Select all fields from populations with records corresponding to larger than 1.15 times the average you calculated in the first task for 2015. 
In other words, change the 100 in the example above with a subquery.*/

-- Select fields
SELECT *
  -- From populations
  FROM populations
-- Where life_expectancy is greater than
WHERE life_expectancy > 1.15 *
  -- 1.15 * subquery
  (SELECT AVG(life_expectancy)
   FROM populations
   WHERE year = 2015)
  AND year = 2015;
-- EXTRACT name,code and pop for all cities that are capitals  
  -- 2. Select fields
SELECT name, country_code, urbanarea_pop
  -- 3. From cities
  FROM cities
-- 4. Where city name in the field of capital cities
WHERE name IN
  -- 1. Subquery
  (SELECT capital
   FROM  countries)
ORDER BY urbanarea_pop DESC;

-- Equivalent queries one using inner join 1st, 2nd one using sub-query

SELECT countries.name AS country, COUNT(*) AS cities_num
  FROM cities
    INNER JOIN countries
    ON countries.code = cities.country_code
GROUP BY country
ORDER BY cities_num DESC, country
LIMIT 9;


SELECT name AS country,
  (SELECT COUNT(*)
   FROM cities
   WHERE countries.code = cities.country_code) AS cities_num
FROM countries
ORDER BY cities_num DESC, country
LIMIT 9;

-- Select fields (with aliases)
SELECT code,count(*) as lang_num
  -- From languages
  from languages
-- Group by code
Group BY code;

-- Select fields
SELECT local_name,lang_num
  -- From countries
  FROM countries,
  	-- Subquery (alias as subquery)
  	(SELECT code,COUNT(*) as lang_num
  	 FROM languages
  	 GROUP BY code) AS t
  -- Where codes match
  WHERE countries.code = t.code
-- Order by descending number of languages
ORDER BY lang_num DESC;


-- Select fields
SELECT name,continent,inflation_rate
  -- From countries
  FROM countries
  	-- Join to economies
  	INNER JOIN economies
    -- Match on code
    --ON countries.code = economies.code
    USING(code)
-- Where year is 2015
WHERE year = 2015;

-- Select fields
SELECT MAX(inflation_rate) as max_inf
  -- Subquery using FROM (alias as subquery)
  FROM (
      SELECT name,continent,inflation_rate
      FROM countries
      INNER JOIN economies
      USING(code)
      WHERE year = 2015) AS subquery
-- Group by continent
GROUP BY continent;

-- Select fields
SELECT name,continent,inflation_rate
  -- From countries
  from countries
	-- Join to economies
	INNER JOIN economies
	-- Match on code
	ON countries.code = economies.code
  -- Where year is 2015
  WHERE year = 2015
    
    -- And inflation rate in subquery (alias as subquery)
    AND inflation_rate IN (
        -- Select fields    
    SELECT MAX(inflation_rate) as max_inf
        -- Subquery using FROM (alias as subquery)
    FROM (
            SELECT name,continent,inflation_rate
            FROM countries
            INNER JOIN economies
            USING(code)
            WHERE year = 2015) AS subquery
        -- Group by continent
        GROUP BY continent);
		
-- SMALLE VERSION IMO
SELECT name,continent,inflation_rate
  -- From countries
  from countries
	-- Join to economies
	INNER JOIN economies
	-- Match on code
	ON countries.code = economies.code
  -- Where year is 2015
  WHERE year = 2015
  AND inflation_rate IN (
  	        SELECT MAX(inflation_rate)
            FROM countries
            INNER JOIN economies
            USING(code)
            WHERE year = 2015
	        GROUP BY continent);
			
/*Select the country code, inflation rate, and unemployment rate.
Order by inflation rate ascending. */			
-- Select fields
SELECT code, inflation_rate, unemployment_rate
  -- From economies
  FROM economies
  -- Where year is 2015 and code is not in
  WHERE year = 2015 AND code NOT IN
  	-- Subquery
  	(SELECT code
  	 FROM countries
  	 WHERE (gov_form = 'Constitutional Monarchy' OR gov_form LIKE '%Republic%'))
-- Order by inflation rate
ORDER BY inflation_rate;    

SELECT DISTINCT c.name, e.total_investment, e.imports
  -- From table (with alias)
  FROM countries AS c
    -- Join with table (with alias)
    LEFT JOIN economies AS e
      -- Match on code
      ON (c.code = e.code);
-- Select fields
SELECT DISTINCT c.name, total_investment, imports
  -- From table (with alias)
  FROM countries AS c
    -- Join with table (with alias)
    LEFT JOIN economies AS e
      -- Match on code
      ON (c.code = e.code)
      -- and code in Subquery
        AND c.code IN (
          SELECT l.code
          FROM languages AS l
          WHERE l.official = 'true'
        ) 
  -- Where region and year are correct
  WHERE c.region = 'Central America' AND  year = 2015
-- Order by field
ORDER BY c.name;

-- Select fields
SELECT c.region,c.continent, AVG(p.fertility_rate) AS avg_fert_rate
  -- From left table
  FROM countries AS c
    -- Join to right table
    INNER JOIN populations AS p
      -- Match on join condition
      ON c.code = p.country_code
  -- Where specific records matching some condition
  WHERE year = 2015
-- Group appropriately
GROUP BY region, continent
-- Order appropriately
ORDER BY avg_fert_rate;

/*
    Select the city name, country code, city proper population, and metro area population.
    Calculate the percentage of metro area population composed of city proper population for each city in cities, aliased as city_perc.
    Focus only on capital cities in Europe and the Americas in a subquery.
    Make sure to exclude records with missing data on metro area population.
    Order the result by city_perc descending.
    Then determine the top 10 capital cities in Europe and the Americas in terms of this city_perc percentage.
*/
-- Select fields
SELECT name, country_code,city_proper_pop,metroarea_pop,  
      -- Calculate city_perc
      (city_proper_pop / metroarea_pop) * 100 AS city_perc
  -- From appropriate table
  FROM cities
  -- Where 
  WHERE name IN
    -- Subquery
    (SELECT capital
     FROM countries
     WHERE (continent = 'Europe'
        OR continent LIKE '%America%'))
       AND metroarea_pop IS NOT NULL
-- Order appropriately
ORDER BY city_perc DESC
-- Limit amount
LIMIT (10);