-- Select fields from 2010 table
SELECT *
  -- From 2010 table
  FROM economies2010
	-- Set theory clause
	UNION
-- Select fields from 2015 table
SELECT *
  -- From 2015 table
  FROM economies2015
-- Order by code and year
ORDER BY code, year;

-- Select field
SELECT country_code
  -- From cities
  FROM cities
	-- Set theory clause
	UNION
-- Select field
SELECT code as country_code
  -- From currencies
  FROM currencies
-- Order by country_code
ORDER BY country_code;


-- Select fields
SELECT code, year
  -- From economies
  FROM economies
	-- Set theory clause
	UNION ALL
-- Select fields
SELECT country_code As code, year
  -- From populations
  FROM populations
-- Order by code, year
ORDER BY code, year;

-- Select fields
SELECT code,year
  -- From economies
  from economies
	-- Set theory clause
	INTERSECT
-- Select fields
SELECT country_code,year
  -- From populations
  FROM populations
-- Order by code and year
ORDER BY code,year;


-- Select fields
SELECT name
  -- From countries
  FROM countries
	-- Set theory clause
	INTERSECT
-- Select fields
SELECT name
  -- From cities
  from cities;
  
-- Select field
SELECT name as city -- Correct in relation algebra
  -- From cities
  FROM cities
	-- Set theory clause
	EXCEPT
-- Select field
SELECT capital as city
  -- From countries
  FROM countries
-- Order by result
ORDER BY city;


-- Select field
SELECT capital
  -- From countries
  FROM countries
	-- Set theory clause
	EXCEPT
-- Select field
SELECT name
  -- From cities
  from cities
-- Order by ascending capital
order by capital;

-- Select distinct fields
SELECT DISTINCT name
  -- From languages
  FROM languages
-- Where in statement
WHERE code IN
  -- Subquery
  (SELECT code
   FROM countries
   WHERE region = 'Middle East')
-- Order by name
ORDER BY name;

SELECT DISTINCT name
FROM languages
WHERE code IN
  (SELECT code
   FROM countries
   WHERE region = 'Middle East')
ORDER BY name;

SELECT DISTINCT languages.name AS language
FROM languages
INNER JOIN countries
ON languages.code = countries.code
WHERE region = 'Middle East'
ORDER BY language;

-- 3. Select fields
SELECT name,code 
  -- 4. From Countries
  from countries
  -- 5. Where continent is Oceania
  where continent = 'Oceania'
  	-- 1. And code not in
  AND code NOT IN
  	-- 2. Subquery
  	(SELECT code 
  	 FROM currencies);

/* 
    Identify the country codes that are included in either economies or currencies but not in populations.
    Use that result to determine the names of cities in the countries that match the specification in the previous instruction.
*/
-- Select the city name
SELECT c1.name
  -- Alias the table where city name resides
  FROM cities AS c1
  -- Choose only records matching the result of multiple set theory clauses
  WHERE c1.country_code IN
(
    -- Select appropriate field from economies AS e
    SELECT e.code
    FROM economies AS e
    -- Get all additional (unique) values of the field from currencies AS c2  
    UNION
    SELECT DISTINCT c2.code
    FROM currencies AS c2
    -- Exclude those appearing in populations AS p
    EXCEPT 
    SELECT p.country_code
    FROM populations AS p
);