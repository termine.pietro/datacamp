-- 4. Select fields
SELECT c.name as country, continent,l.name as language,l.official
  -- 1. From countries (alias as c)
  FROM countries as c
  -- 2. Join to languages (as l)
  INNER JOIN languages as l
    -- 3. Match using code
    using(code)