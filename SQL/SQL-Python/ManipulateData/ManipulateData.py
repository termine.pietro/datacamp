import numpy as np
import pandas as pd

df = pd.read_csv('Match.csv')
names = ['id','goal','shoton','shotoff','foulcommit','card','cross','corner','possession']
indices = [ 'id','country_id', 'league_id', 'season', 'stage', 'date',
            'match_api_id', 'home_team_api_id', 'away_team_api_id',
            'home_team_goal','away_team_goal']
cols_new_names = ['id','country_id','league_id','season','stage','date','match_api_id','hometeam_id','awayteam_id','home_goal','away_goal']
for n in names:
    print(df[n].iloc[3134:3140])
print(df.head())
print(df.columns)

df_redux = df[indices]
cols = {indices[i]:cols_new_names[i]  for i in range(len(indices))}
print(cols)
df_redux.rename(columns=cols, inplace=True)
df_redux.set_index('id',inplace=True)
print(df_redux.head())

df_redux.to_csv('Match_redux.csv')