SELECT PlayerName, 
       Country,
       ROUND(Weight_kg/SQUARE(Height_cm/100),2) AS BMI
FROM Players 
WHERE Country = 'USA'
      OR Country = 'Canada'
ORDER BY BMI;

/*
   Returns the Body Mass Index (BMI) for all North American players from the 2017-2018 NBA season
   from the 2017-2018 NBA season
*/
SELECT PlayerName, Country,
    ROUND(Weight_kg/SQUARE(Height_cm/100),2) BMI 
FROM Players 
WHERE Country = 'USA'
    OR Country = 'Canada';
--ORDER BY BMI; -- Order by not required

-- Your friend's query
-- First attempt, contains errors and inconsistent formatting
/*
select PlayerName, p.Country,sum(ps.TotalPoints) 
AS TotalPoints  
FROM PlayerStats ps inner join Players On ps.PlayerName = p.PlayerName
WHERE p.Country = 'New Zeeland'
Group 
by PlayerName, Country
order by Country;
*/

-- Your query


SELECT p.PlayerName, p.Country,
		SUM(ps.TotalPoints) AS TotalPoints  
FROM PlayerStatistics ps 
INNER JOIN Players p
	ON ps.PlayerName = p.PlayerName
WHERE p.Country = 'New Zealand'
GROUP BY p.PlayerName, p.Country;

-- Join example
SELECT p.PlayerName, p.Country,
         SUM(ps.TotalPoints) AS TotalPoints  
FROM PlayerStatistics as ps
INNER JOIN Players as p
   ON ps.PlayerName = p.PlayerName
WHERE p.Country = 'Australia'
GROUP BY p.PlayerName, p.Country


SELECT Team, 
   ROUND(AVG(BMI),2) AS AvgTeamBMI -- Alias the new column
FROM PlayerStatistics as ps -- Alias PlayerStats table
INNER JOIN
		(SELECT PlayerName, Country,
			Weight_kg/SQUARE(Height_cm/100) as BMI
		 FROM Players) as PlayerStats -- Alias the sub-query
             -- Alias the joining columns
	ON ps.PlayerName = PlayerStats.PlayerName 
GROUP BY Team
HAVING AVG(BMI) >= 25;