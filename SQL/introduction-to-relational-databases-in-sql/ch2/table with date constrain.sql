-- table with date constrain the orginal was 
/*CREATE TABLE transactions (
 transaction_date date, 
 amount integer,
 fee text
);*/

-- Let's add a record to the table
INSERT INTO transactions (transaction_date, amount, fee) 
--VALUES ('2018-24-09', 5454, '30');

VALUES ('2018-09-24', 5454, '30');

-- Doublecheck the contents
SELECT *
FROM transactions;