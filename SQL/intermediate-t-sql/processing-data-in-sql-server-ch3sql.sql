-- Declare the variable (a SQL Command, the var name, the datatype)
DECLARE @counter INT 

-- Set the counter to 20
SET @counter = 20

-- Select and increment the counter by one 
SET @counter = @counter + 1

-- Print the variable
SELECT @counter

--DECLARE @counter INT 
SET @counter = 20

-- Create a loop
WHILE (@counter <30)

-- Loop code starting point
BEGIN
	SELECT @counter = @counter + 1
-- Loop finish
END

-- Check the value of the variable
SELECT @counter

-- Derived tables
SELECT a.RecordId, a.Age, a.BloodGlucoseRandom, 
-- Select maximum glucose value (use colname from derived table)    
       b.MaxGlucose
FROM Kidney a
-- Join to derived table
JOIN (SELECT Age, MAX(BloodGlucoseRandom) AS MaxGlucose FROM Kidney GROUP BY Age) b
-- Join on Age
ON b.Age = a.Age;

SELECT Age,MAX(BloodPressure) as MaxBloodPressure FROM Kidney GROUP BY Age;

SELECT *
FROM Kidney a
-- Create derived table: select age, max blood pressure from kidney grouped by age
JOIN (SELECT Age,MAX(BloodPressure) as MaxBloodPressure FROM Kidney GROUP BY Age) b
-- JOIN on BloodPressure equal to MaxBloodPressure
ON a.BloodPressure = b.MaxBloodPressure
-- Join on Age
AND a.Age = b.Age;

-- Common Table Expressions CTEs
-- Specify the keyowrds to create the CTE
WITH BloodGlucoseRandom (MaxGlucose) 
as (SELECT MAX(BloodGlucoseRandom) AS MaxGlucose FROM Kidney)

SELECT a.Age, b.MaxGlucose
FROM Kidney a
-- Join the CTE on blood glucose equal to max blood glucose
JOIN BloodGlucoseRandom b
ON a.BloodGlucoseRandom = b.MaxGlucose;  

-- Create the CTE
WITH BloodPressure(MaxBloodPressure) -- Requires the format of the new table with prper names
AS (SELECT Max(BloodPressure) as MaxBloodPressure FROM Kidney)

SELECT *
FROM Kidney a
-- Join the CTE  
JOIN BloodPressure as b
ON a.BloodPressure = b.MaxBloodPressure;