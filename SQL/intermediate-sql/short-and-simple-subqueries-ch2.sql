-- Select the average of home + away goals, multiplied by 3
/*SELECT 
	3 * AVG(home_goal + away_goal)
FROM matches_2013_2014;*/

-- This gives exaclty the same results as in the datacamp exercise above
SELECT 
	3 * AVG(CAST(home_goal + away_goal as float))
FROM (SELECT * FROM match WHERE season = '2013.2014') as matches_2013_2014;

SELECT 
	-- Select the date, home goals, and away goals scored
    date,
	home_goal,
	away_goal
FROM (SELECT * FROM match WHERE season = '2013.2014') as matches_2013_2014
-- Filter for matches where total goals exceeds 3x the average
WHERE (home_goal + away_goal) > 
       (SELECT 3 * AVG(CAST(home_goal + away_goal as float))
        FROM (SELECT * FROM match WHERE season = '2013.2014')as matches_2013_2014); 

-- USING Common Table they nned to be used immidietly in another statement 
-- OTHERWISE Syntax Error
with matches_2013_2014 as
(	  SELECT * 
	  FROM match 
	  WHERE season = '2013.2014')
SELECT 
	-- Select the date, home goals, and away goals scored
    date,
	home_goal,
	away_goal
FROM matches_2013_2014
-- Filter for matches where total goals exceeds 3x the average
WHERE (home_goal + away_goal) > 
       (SELECT 3 * AVG(CAST(home_goal + away_goal as float))
        FROM  matches_2013_2014); 

-- this returns empty set since all values are present 
-- using where clause to get a copmarable subset 
SELECT 
	-- Select the team long and short names
	team_long_name,
	team_short_name,
	team_api_id
FROM team 
-- Exclude all values from the subquery
WHERE team_api_id NOT IN 
     (SELECT DISTINCT hometeam_id  FROM match where hometeam_id > 7000);

-- this returns empty set since all values are present 
-- using where clause to get a copmarable subset 
SELECT 
	-- Select the team long and short names
	team_long_name,
	team_short_name,
	team_api_id
FROM team 
-- Exclude all values from the subquery
WHERE EXISTS   
     (SELECT DISTINCT hometeam_id  FROM match where hometeam_id > 7000);


 /*   Create a subquery in WHERE clause that retrieves all hometeam_ID values from match with a home_goal score greater than or equal to 8.
    Select the team_long_name and team_short_name from the team table. Include all values from the subquery in the main query.
*/
SELECT
	-- Select the team long and short names
	team_long_name,
	team_short_name
FROM team
-- Filter for teams with 8 or more home goals
WHERE team_api_id IN
	  (SELECT hometeam_ID 
       FROM match
       WHERE home_goal >= 8);

SELECT 
	-- Select the country ID and match ID
	id, 
    country_id 
FROM match
-- Filter for matches with 10 or more goals in total
WHERE (home_goal + away_goal) >= 10;

SELECT
	-- Select country name and the count match IDs
    c.name AS country_name,
    COUNT(sub.id) AS matches
FROM country AS c
-- Inner join the subquery onto country
-- Select the country id and match id columns
INNER JOIN (SELECT id, country_id 
           FROM match
           -- Filter the subquery by matches with 10+ goals
           WHERE (home_goal + away_goal) >= 10) AS sub
ON c.id = sub.country_id
GROUP BY c.name;

SELECT
	-- Select country, date, home, and away goals from the subquery
    subq.country,
    date,
    home_goal,
    away_goal
FROM 
	-- Select country name, date, and total goals in the subquery
	(SELECT c.name AS country, 
     	    m.date, 
     		m.home_goal, 
     		m.away_goal,
           (m.home_goal + m.away_goal) AS total_goals
    FROM match AS m
    LEFT JOIN country AS c
    ON m.country_id = c.id) AS subq
-- Filter by total goals scored in the main query
WHERE total_goals >= 10;

SELECT 
	l.name AS league,
    -- Select and round the league's total goals
    ROUND(AVG(CAST(m.home_goal + m.away_goal as float)), 2) AS avg_goals,
    -- Select & round the average total goals for the season
    (SELECT ROUND(AVG(CAST(home_goal + away_goal as float)), 2) 
     FROM match
     WHERE season = '2013.2014') AS overall_avg
FROM league AS l
LEFT JOIN match AS m
ON l.country_id = m.country_id
-- Filter for the 2013/2014 season
WHERE m.season = '2013.2014'
GROUP BY l.name;

/*

    Select the average goals scored in a match for each league in the main query.
    Select the average goals scored in a match overall for the 2013/2014 season in the subquery.
    Subtract the subquery from the average number of goals calculated for each league.
    Filter the main query so that only games from the 2013/2014 season are included.

*/
SELECT
	-- Select the league name and average goals scored
	l.name AS league,
	ROUND(AVG(CAST(m.home_goal + m.away_goal as float)),2) AS avg_goals,
    -- Subtract the overall average from the league average
	ROUND(AVG(m.home_goal + m.away_goal) - 
		(SELECT AVG(CAST(home_goal + away_goal as float))
		 FROM match 
         WHERE season = '2013.2014'),2) AS diff
FROM league AS l
LEFT JOIN match AS m
ON l.country_id = m.country_id
-- Only include 2013/2014 results
WHERE m.season = '2013.2014'
GROUP BY l.name;

/*

    Extract the average number of home and away team goals in two SELECT subqueries.
    Calculate the average home and away goals for the specific stage in the main query.
    Filter both subqueries and the main query so that only data from the 2012/2013 season is included.
    Group the query by the m.stage column.

*/

SELECT 
	-- Select the stage and average goals for each stage
	m.stage,
    ROUND(AVG(CAST(m.home_goal + m.away_goal as float)),2) AS avg_goals,
    -- Select the average overall goals for the 2012/2013 season
    ROUND((SELECT AVG(CAST(home_goal + away_goal as float)) 
           FROM match 
           WHERE season = '2012.2013'),2) AS overall
FROM match AS m
-- Filter for the 2012/2013 season
WHERE m.season = '2012.2013'
-- Group by stage
GROUP BY m.stage
ORDER BY m.stage;

/*

    Calculate the average home goals and average away goals from the match table for each stage in the FROM clause subquery.
    Add a subquery to the WHERE clause that calculates the overall average home goals.
    Filter the main query for stages where the average home goals is higher than the overall average.
    Select the stage and avg_goals columns from the s subquery into the main query.

*/
SELECT 
	-- Select the stage and average goals from the subquery
	stage,
	ROUND(s.avg_goals,2) AS avg_goals
FROM 
	-- Select the stage and average goals in 2012/2013
	(SELECT
		 stage,
         AVG(CAST(home_goal + away_goal AS float)) AS avg_goals
	 FROM match
	 WHERE season = '2012.2013'
	 GROUP BY stage) AS s
WHERE 
	-- Filter the main query using the subquery
	s.avg_goals > (SELECT AVG(CAST(home_goal + away_goal as float)) 
                    FROM match WHERE season = '2012.2013');