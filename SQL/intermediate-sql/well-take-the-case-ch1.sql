SELECT 
	c.name AS country,
    -- Count games from the 2012/2013 season
	count(case when m.season = '2012.2013'
        	then m.id ELSE NULL END) AS matches_2012_2013
FROM country AS c
LEFT JOIN match AS m
ON c.id = m.country_id
-- Group by country name alias
GROUP BY c.name;

SELECT 
	c.name AS country,
    -- Count matches in each of the 3 seasons
	COUNT(CASE WHEN m.season = '2012.2013' THEN m.id ELSE NULL END) AS matches_2012_2013,
	COUNT(CASE WHEN m.season = '2013.2014' THEN m.id ELSE NULL END) AS matches_2013_2014,
	COUNT(CASE WHEN m.season = '2014.2015' THEN m.id ELSE NULL END) AS matches_2014_2015
FROM country AS c
LEFT JOIN match AS m
ON c.id = m.country_id
-- Group by country name alias
GROUP BY c.name;

SELECT 
	c.name AS country,
    -- Sum the total records in each season where the home team won
	SUM(CASE WHEN m.season = '2012.2013' AND m.home_goal > m.away_goal 
        THEN 1 ELSE 0 END) AS matches_2012_2013,
 	SUM(CASE WHEN m.season = '2013.2014' AND m.home_goal > m.away_goal 
        THEN 1 ELSE 0 END) AS matches_2013_2014,
	SUM(CASE WHEN m.season = '2014.2015' AND m.home_goal > m.away_goal 
        THEN 1 ELSE 0 END) AS matches_2014_2015
FROM country AS c
LEFT JOIN match AS m
ON c.id = m.country_id
-- Group by country name alias
GROUP BY c.name;

/*  Create 3 CASE statements to COUNT the total number of home team wins, away team wins, and ties. 
	This will allow you to examine the total number of records. 
	You will convert this to an AVG in the next step*/
SELECT 
    c.name AS country,
    -- Count the home wins, away wins, and ties in each country
	COUNT(CASE WHEN m.home_goal > m.away_goal THEN m.id 
        END) AS home_wins,
	COUNT(CASE WHEN m.home_goal < m.away_goal THEN m.id 
        END) AS away_wins,
    COUNT(CASE WHEN m.home_goal = m.away_goal THEN m.id 
        END) AS ties
FROM country AS c
LEFT JOIN match AS m
ON c.id = m.country_id
GROUP BY c.name;

/*
    Calculate the percentage of matches tied using AVG inside a CASE statement.
    Fill in the logical operators for each statement. Alias your columns as ties_2013_2014
	and ties_2014_2015, respectively.
*/
-- DOen;t work in sql server needs debug: attributes are ins then either requires expilicit cast to float or just putting 
-- 1.0 instead of 1 
SELECT 
	c.name AS country,
    -- Calculate the percentage of tied games in each season
	AVG(case WHEN m.season='2013.2014' AND m.home_goal = m.away_goal THEN 1.0
			 WHEN m.season='2013.2014' AND m.home_goal != m.away_goal THEN 0.0
			END) AS ties_2013_2014,
	AVG(CAST(CASE WHEN m.season='2014.2015' AND m.home_goal = m.away_goal THEN 1
			 WHEN m.season='2014.2015' AND m.home_goal != m.away_goal THEN 0
			END as float)) AS ties_2014_2015
FROM Country AS c
LEFT JOIN match AS m
ON c.id = m.country_id
GROUP BY c.name;

SELECT 
	c.name AS country,
    -- Round the percentage of tied games to 2 decimal points
	ROUND(AVG(CASE WHEN m.season='2013.2014' AND m.home_goal = m.away_goal THEN 1.0
			 WHEN m.season='2013.2014' AND m.home_goal != m.away_goal THEN 0.0
			 END),2) AS pct_ties_2013_2014,
	ROUND(AVG(CASE WHEN m.season='2014.2015' AND m.home_goal = m.away_goal THEN 1.0
			 WHEN m.season='2014.2015' AND m.home_goal != m.away_goal THEN 0.0
			 END),2) AS pct_ties_2014_2015
FROM country AS c
LEFT JOIN match AS m
ON c.id = m.country_id
GROUP BY c.name;