SELECT * 
FROM people;

SELECT name 
FROM people;

-- Try running me!
SELECT 'DataCamp <3 SQL' 
AS result;

SELECT DISTINCT certification
FROM films;

SELECT DISTINCT role
FROM roles;

SELECT COUNT(*)
FROM reviews;
