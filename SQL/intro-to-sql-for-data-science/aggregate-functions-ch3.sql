SELECT MAX(duration) as max,
       AVG(duration) as avg,
       SUM(duration) as sum 

FROM films;

SELECT MAX(gross)
FROM films
WHERE release_year BETWEEN 2000 and 2012;


--Get the title and net profit (the amount a film grossed, minus its budget) 
--for all films. Alias the net profit as net_profit.
Select title,
       gross - budget as net_profit
FROM films;

-- Get the title and duration in hours for all films. The duration is in minutes,
-- so you'll need to divide by 60.0 to get the duration in hours.
-- Alias the duration in hours as duration_hours
Select title,
       gross - budget as net_profit
FROM films;

Select 
    AVG(duration)/60.0 as avg_duration_hours
FROM films;


-- get the count(deathdate) and multiply by 100.0
-- then divide by count(*)

Select 
    count(deathdate)*100.0/count(*) as percentage_dead
FROM people;

Select 
    MAX(release_year) - MIN(release_year) as difference
FROM films;

-- Get the number of decades the films table covers. 
-- Alias the result as number_of_decades. 
-- The top half of your fraction should be enclosed in parentheses.
Select 
    (MAX(release_year) - MIN(release_year))/10.0 as number_of_decades
FROM films;