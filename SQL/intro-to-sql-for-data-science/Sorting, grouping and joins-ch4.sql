Select name
FROM people
Order By name;

Select birthdate,name
FROM people
Order By birthdate;

Select title
FROM films
WHERE release_year IN (2000 , 2012)
Order By release_year;

-- Get all details for all films except 
-- those released in 2015 and order them by duration.
Select *
FROM films
WHERE release_year != 2015 
Order By duration;

Select title,gross
FROM films
WHERE title like 'M%' 
Order By title;

Select imdb_score,film_id
FROM reviews
Order By imdb_score DESC;

-- Get the title and duration for every film, 
-- in order of longest duration to shortest.
Select title,duration
FROM films
Order By duration DESC;

SELECT birthdate, name
FROM people
ORDER BY birthdate, name;

SELECT certification,release_year, title 
FROM films
ORDER BY certification, release_year;

SELECT name,birthdate 
FROM people
ORDER BY name,birthdate;

-- Get the release year and count of films released in each year.

SELECT release_year, count(*)
FROM films
GROUP BY release_year;

--Get the release year and average duration of all films, grouped by release year.

SELECT release_year,AVG(duration/1.0) as Avg
FROM films
GROUP BY release_year;

SELECT release_year,MAX(budget) 
FROM films
GROUP BY release_year;

--Get the IMDB score and count of film reviews grouped by IMDB score in the reviews table.
SELECT imdb_score,COUNT(*) 
FROM reviews
GROUP BY imdb_score
ORDER BY imdb_score DESC;

SELECT language,SUM(gross) 
FROM films
GROUP BY language;

-- Get the release year, country, and highest budget spent making a film 
-- for each year, for each country. Sort your results by release year and country.
SELECT release_year,country,MAX(budget) 
FROM films
GROUP BY release_year,country
ORDER BY release_year,country;



SELECT country,release_year,MIN(gross) 
FROM films
GROUP BY release_year,country
ORDER BY country,release_year;

-- Using Having
SELECT release_year,AVG(budget) as avg_budget,
        AVG(gross) as avg_gross
FROM films
GROUP BY release_year
HAVING AVG(budget) > 60000000;

SELECT release_year,AVG(budget) as avg_budget,
        AVG(gross) as avg_gross
FROM films
GROUP BY release_year
HAVING AVG(budget) > 60000000
ORDER BY avg_gross DESC;


-- select country, average budget, average gross
SELECT country,AVG(budget) as avg_budget,
        AVG(gross) as avg_gross
-- from the films table
FROM films
-- group by country 
GROUP by country
-- where the country has more than 10 titles
HAVING count(country) > 10 
-- order by country
ORDER BY country
-- limit to only show 5 results
LIMIT 7;
