SELECT *
FROM films
where release_year = 2016;

SELECT *
FROM films
where language = 'French';

SELECT name,birthdate
FROM people
where birthdate = '1974-11-11';

SELECT count(*)
FROM films
where language = 'Hindi';

SELECT *
FROM films
where certification = 'R';

SELECT *
FROM films
where language = 'Spanish' And release_year > 2000;

SELECT *
FROM films
where language = 'Spanish' And release_year > 2000 
     And release_year < 2010;

SELECT title,release_year
FROM films
where release_year >= 1990 
     And release_year < 2000 And 
     (language = 'Spanish' OR language = 'French') 
     and gross > 2000000;

SELECT title,release_year
FROM films
WHERE release_year BETWEEN 1990 AND 2000
and budget > 100000000 and 
(language = 'Spanish' OR language = 'French');

SELECT title,language
FROM films
WHERE language in ('English' , 'French','Spanish');	

SELECT title,certification
FROM films
WHERE certification in ('NC-17' , 'R');

SELECT title,certification
FROM films
WHERE certification in ('NC-17' , 'R');

SELECT COUNT(*)
FROM films
where language IS NULL;

-- Get the names of people whose 
-- names have 'r' as the second letter. The pattern you need is '_r%'.
SELECT name
FROM people
where name LIKE '__r%';

--Get the names of people whose names don't start with A. The pattern you need is 'A%'.
SELECT name
FROM people
where name NOT LIKE 'A%';

