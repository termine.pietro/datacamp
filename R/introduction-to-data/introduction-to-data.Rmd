---
title: "language-of-data-ch1"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library('dplyr')
library('openintro')
# Load data
data(email50)

# View the structure of the data
str(email50)

# Glimpse email50
glimpse(email50)
```

```{r}
# Subset of emails with big numbers: email50_big
email50_big <- email50 %>%
  filter(number == 'big')

# Glimpse the subset
glimpse(email50_big)
```

```{r}

# Table of the number variable
table(email50_big$number)

# Drop levels eq to 0
email50_big$number <- droplevels(email50_big$number)

# Another table of the number variable
table(email50_big$number)

```

```{r}
# Calculate median number of characters: med_num_char
med_num_char <- median(email50$num_char)

# Create num_char_cat variable in email50
email50_fortified <- email50 %>%
  mutate(num_char_cat = ifelse(num_char < med_num_char, "below median", "at or above median"))
  
# Count emails in each category
email50_fortified %>%
  count(num_char_cat)
```

```{r}

# Create number_yn column in email50
email50_fortified <- email50 %>%
  mutate(
    number_yn = case_when(
      # if number is "none", make number_yn "no"
      number == "none" ~ "no", 
      # if number is not "none", make number_yn "yes"
      number != 'none' ~ "yes"  
    )
  )
  

# Visualize the distribution of number_yn
ggplot(email50_fortified, aes(x = number_yn)) +
  geom_bar()
```

```{r}
# Load ggplot2
library(ggplot2)

# Scatterplot of exclaim_mess vs. num_char
ggplot(email50, aes(x = num_char, y = exclaim_mess, color = factor(spam))) +
  geom_point()
```

```{r}
# Load data
data(gapminder)

# Glimpse data
glimpse(gapminder)

# Identify type of study: observational or experimental
type_of_study <- "observational"
```

Chapter 2
```{r}
# Load packages
library(dplyr)
load('ucb_admit.RData')
# Count number of male and female applicants admitted
ucb_admission_counts = ucb_admit %>%
  count(Gender, Admit)
```
```{r}

ucb_admission_counts %>%
  # Group by gender
  group_by(Gender) %>%
  # Create new variable
  mutate(prop = n / sum(n)) %>%
  # Filter for admitted
  filter(Admit== "Admitted")
```

```{r}
ucb_admission_counts <- ucb_admit %>%
  # Counts by department, then gender, then admission status
  count(Dept, Gender, Admit)

# See the result
ucb_admission_counts
```

```{r}
ucb_admission_counts  %>%
  # Group by department, then gender
  group_by(Dept, Gender) %>%
  # Create new variable
  mutate(prop = n / sum(n)) %>%
  # Filter for male and admitted
  filter(Gender == "Male", Admit == "Admitted")
```

