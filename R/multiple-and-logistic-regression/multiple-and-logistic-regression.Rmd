---
title: "multiple-and-logistic-regression"
author: "Pietro Termine"
date: "21/01/2020"
output: html_document
---

```{r setup}
library('openintro') # mario_kart dataset
library('ggplot2')
library('tidyr')
library('dplyr')
library('mosaicData') # Galton dataset
library('broom')
library('modelr') # data_grid
library('plotly')
library('Stat2Data') #Med2... dataset
```

## Multivariate LM Continous + Categorical (factor)


```{r}
mario_kart <- marioKart %>% filter(totalPr < 100) # removing outliers
# Explore the data
glimpse(mario_kart)

# fit parallel slopes
mod <- lm(totalPr ~ wheels +cond,data=mario_kart)
```

## Including Plots

You can also embed plots, for example:

```{r}
# Augment the model
augmented_mod <- augment(mod)
glimpse(augmented_mod)

# scatterplot, with color
data_space <- ggplot(augmented_mod, aes(x = wheels, y = totalPr, color = cond)) + 
  geom_point()
  
# single call to geom_line()
data_space + 
  geom_line(aes(y = .fitted))
```



```{r}
# build model
mod1 = lm(bwt ~ age + parity,data=babies)
# build model

mod2 = lm(bwt ~ gestation + smoke,data=babies)
```

```{r}
# R^2 and adjusted R^2
summary(mod)

# add random noise
mario_kart_noisy <- mario_kart %>% mutate(noise = rnorm(dim(mario_kart)[1]))
  
# compute new model
mod2 <- lm(formula = totalPr ~ wheels + cond + noise, data = mario_kart_noisy)

# new R^2 and adjusted R^2
summary(mod2)

```

# Predict values
```{r}
# return a vector
predict(mod)

# return a data frame
augment(mod)
```

# Variable interaction: non parallel lines x1*x2 or X1 + x2
```{r}
# include interaction
mod3 = lm(formula = totalPr ~ duration + cond + duration:cond , data = mario_kart)
```

```{r}
# interaction plot
ggplot(mario_kart, aes(y=totalPr, x=duration,color=cond)) + 
  geom_point() + 
  geom_smooth(method='lm',se=F)
```

Simpson's Paradox : direction of correlation changes if groups are taken under consideration

```{r}
ggplot(mario_kart, aes(y = totalPr, x = duration)) + 
  geom_point() + 
  geom_smooth(method = "lm", se = FALSE)

# model with one slope
lm(totalPr ~ duration,data=mario_kart)

# plot with two slopes
ggplot(mario_kart, aes(y = totalPr, x = duration,color=cond)) + 
  geom_point() + 
  geom_smooth(method = "lm", se = FALSE)
```

```{r}
# Fit the model using duration and startPr
mod = lm(totalPr ~ duration + startPr,data=mario_kart)
```


```{r}
grid <- mario_kart %>%
  data_grid(
      duration = seq_range(duration, by = 1),
      startPr = seq_range(startPr, by = 1)
)

# add predictions to grid
price_hats <- augment(mod,newdata=grid)

data_space <- ggplot(mario_kart, aes(x = duration, y = startPr)) +
  geom_point(aes(color = totalPr))

# tile the plane
data_space + 
  geom_tile(data = price_hats, aes(fill=.fitted),alpha=0.5)
```

3d plot: Missing planes obtained by using the linear model 

```{r}
# draw the 3D scatterplot
#p <-
plot_ly(data = mario_kart, z = ~totalPr, x = ~duration, y = ~startPr, opacity = 0.6) %>%
  add_markers() 
  
# draw the plane
#p %>%
 # add_surface(x = ~x, y = ~y, z = ~plane, showscale = FALSE)
```

```{r}
# draw the 3D scatterplot
p <- plot_ly(data = mario_kart, z = ~totalPr, x = ~duration, y = ~startPr, opacity = 0.6) %>%
  add_markers(color = ~cond) 
  
# draw two planes
p %>%
  add_surface(x = ~x, y = ~y, z = ~plane0, showscale = FALSE) %>%
  add_surface(x = ~x, y = ~y, z = ~plane1, showscale = FALSE)
```


## logistic regression
```{r}
data('MedGPA')
# scatterplot with jitter
data_space <- ggplot(MedGPA,aes(x=GPA,y=Acceptance)) + 
  geom_jitter(width = 0, height = 0.05, alpha = 0.5)

# linear regression line
data_space + 
  geom_smooth(method='lm',se=F)
```

```{r}
# filter
MedGPA_middle <- MedGPA %>% filter (GPA>= 3.375 & GPA <=3.77)

# scatterplot with jitter
data_space <- ggplot(MedGPA_middle,aes(x=GPA,y=Acceptance)) + 
  geom_jitter(width = 0, height = 0.05, alpha = 0.5)

# linear regression line
data_space + 
  geom_smooth(method='lm',se=F)
```

```{r}
# fit model
mod = glm(Acceptance ~ GPA, data = MedGPA, family = binomial)
```
```{r}
# scatterplot with jitter
data_space <- ggplot(data=MedGPA,aes(x=GPA,y=Acceptance)) + 
  geom_jitter(width=0,height=0.05, alpha = .5)

# add logistic curve
data_space +
  geom_smooth(method='glm',se=F,method.args=list(family='binomial'))
```
```{r}
bins=c(2.72,3.3,3.44,3.58,3.7,3.87,3.97)
mean_GPA <- tapply(MedGPA$GPA, cut(MedGPA$GPA, bins,include.lowest = T), mean)
rate <-function(x){ sum(x)/length(x)} 
acceptance_rate <- tapply(MedGPA$Acceptance, cut(MedGPA$GPA,bins), rate)

MedGPA_binned= data.frame(mean_GPA,acceptance_rate)
MedGPA_binned
```



```{r}
# binned points and line
data_space <- ggplot(data = MedGPA_binned, aes(x = mean_GPA, y = acceptance_rate)) + 
  geom_point() + geom_line()

# augmented model
MedGPA_plus <- mod %>%
  augment(type.predict = "response")

# logistic model on probability scale
data_space +
  geom_line(data = MedGPA_plus, aes(x = GPA, y = .fitted), color = "red")
```

```{r}
# compute odds for bins
MedGPA_binned <- MedGPA_binned %>%
  mutate(odds = acceptance_rate / (1 - acceptance_rate))

# plot binned odds
data_space <- ggplot(data = MedGPA_binned, aes(x = mean_GPA, y = odds)) + 
  geom_point() + geom_line()

# compute odds for observations
MedGPA_plus <- MedGPA_plus %>%
  mutate(odds_hat = .fitted / (1 - .fitted))

# logistic model on odds scale
data_space +
  geom_line(data = MedGPA_plus, aes(x = GPA, y = odds_hat), color = "red")
```

```{r}
# compute log odds for bins
MedGPA_binned <- MedGPA_binned %>%
  mutate(log_odds = log(odds))
# plot binned log odds
data_space <- ggplot(data = MedGPA_binned, aes(x = mean_GPA, y = log_odds)) + 
  geom_point() + geom_line()

# compute log odds for observations
MedGPA_plus <- MedGPA_plus %>%
  mutate(log_odds_hat = log(odds_hat))

# logistic model on log odds scale
data_space +
  geom_line(data = MedGPA_plus, aes(x = GPA, y = log_odds_hat), color = "red")
```

```{r}
# create new data frame
new_data <- data.frame(GPA=c(3.51))

# make predictions
augment(mod,newdata=new_data,type.predict='response')
```
```{r}
# data frame with binary predictions
tidy_mod <- augment(mod, type.predict = "response") %>% 
  mutate(Acceptance_hat = round(.fitted)) 
  
# confusion matrix
tidy_mod %>% 
  select(Acceptance, Acceptance_hat) %>%
  table()
```

## Italian restaurants in NYC

```{r}
nyc = read.csv('nyc.csv',header = T)
```

EDA

Price = food strongly correlated as well as Price and decor
```{r}
pairs(nyc)
```

```{r}
# Price by Food plot
ggplot(nyc,aes(x=Food,y=Price )) + geom_point()

# Price by Food model
mod = lm(Price ~ Food ,data=nyc)
```

```{r}
# fit model
mod=lm(Price ~ Food + Service,data=nyc)

# draw 3D scatterplot
p <- plot_ly(data = nyc, z = ~Price, x = ~Food, y = ~Service, opacity = 0.6) %>%
  add_markers() 

# draw a plane
p #%>%
  #add_surface(x = ~x, y = ~y, z = ~plane, showscale = FALSE) 
```

Higher dimensions : collinearity and Multi-Collinearity

```{r}
# Price by Food and Service and East
mod3 = lm(Price ~ Food + Service + East,data=nyc)
```

```{r}
# draw 3D scatterplot
p <- plot_ly(data = nyc, z = ~Price, x = ~Food, y = ~Service, opacity = 0.6) %>%
  add_markers(color = ~factor(East)) 

# draw two planes
p #%>%
  #add_surface(x = ~x, y = ~y, z = ~plane0, showscale = FALSE) %>%
  #add_surface(x = ~x, y = ~y, z = ~plane1, showscale = FALSE)
```

```{r}
mod3 = lm(Price ~ Food + Service + Decor +East ,data=nyc)
```

